# Anarchističtí buřiči
- anarchie = bezvládí
- „básníci života a vzdoru“ (příklon k životu - **vitalismus**)
- generace českých spisovatelů z počátku 20. století
- prožitek 1. sv. války --> **antimilitarismus**
- satanismus, **bohémský** a tulácký **život**
- pohled na **skutečnost bez iluzí**
- návaznost na **českou modernu**
- vrcholící **symbolismus** a dekadence
- středisko anarchistů - **Neumannova vila** na Olšanech
- časopis ***Nový Kult***
- **hlavní motivy**: životní ztroskotání
- **jazyk:** přirozenější, hovorovější, jasnější - vliv moderní civilizace


## [Viktor Dyk](/Autori/Viktor_Dyk.md)
-   1877 - 1931
-   Básník, prozaik, dramatik, publicista a nacionalistický politik
-   První světová válka - člen odboje
-   Manifest českých spisovatelů
-   Méně známý bratr Vojtěcha Dyka
-   *Buřiči*, *Milá sedmi loupežníků*, *Devátá vlna*
-   Novela *Krysař*


## Fráňa Šrámek
-   1877 - 1952
-   Básník, prozaik a dramatik
-   Antimilitaristické postoje
-   Dvakrát vězněn (účast na demonstracích, báseň Píšou mi psaní)
-   Bojoval v první světové válce
-   1946 - národní umělec
-   *Života bído, přec tě mám rád, Modrý a rudý, Splav*
-   žil v Písku a Roudnici nad Labem
-   Ovlivněn Impresionismem
-   Každý po něm něco chtěl, ale on chtěl něco jiného


## Stanislav Kostka Neumann
-   1875 - 1947
-   Básník, publicista a překladatel
-   Zatčen pro účast v hnutí Omladiny
-   Komunistický anarchismus
-   Kniha lesů, vod a strání, Sám nejsi nic, Kdo vám tak zcuchal tmavé vlasy
