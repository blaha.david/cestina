# Antoine de Saint-Exupéry
- francouz
- válečný pilot
- 1\. pol. 20. století (1900 - 1944)
- **francouzská [meziválečná próza](/Kapitoly/Mezivalecna_proza.md)** (moderní světová literatura)
	- vliv 1. sv. války
	- existencionální otázky
- *říkali mu „Král Sluníčko“ pro jeho modré oči a blonďaté vlasy*
- už v dětství psal básně a zabýval se i „vynalézáním“ a obdivoval létající piloty
- ve vojenské službě přidělen k letectvu
- za 2. sv. války sestřelen německým letadlem
- Jeho **dílo**:
	- humanistické příběhy
	- čerpal náměty ze svých zkušeností pilota
	- Svět vidí pohledem člověka žijícího v blízkosti ohrožení
	- Jeho příběhy jsou prostoupeny filozofickými úvahami o smyslu lidského života
	- Oslavuje hrdinství, statečnost a obětavost a protestuje proti krutosti a nesmyslnosti, obvykle spjaté s válkou

## [Malý princ](/Dila/Antoine_de_Saint-Exupery/Maly_princ.md)

## Další díla:
- *Země lidí*, *Noční let*, *Válečný pilot* - letecké romány, osobní zážitky, bezprostřední ohrožení života, úvahy o smyslu existence
- *Citadela* - filozofická esej, pouť jedné karavany v poušti, vydaná posmrtně



## Další autoři této doby:
- ### Česko:
	- Karel Čapek
	- Ival Olbracht
	- Eduard Bass
	- Vladislav Vančura
- ### Francie:
	- Romain Rolland
	- Henri Barbusse
	- Guillaume Apollinaire