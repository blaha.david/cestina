# Bohumil Hrabal
- Nym🅱️urk
- pestré zaměstnání - *písař, notář, úředník, skladník, dělník, výpravčí, obchodní cestující*
- fenomenální sluchová paměť - píše co slyšel - hovorový, nespisovný, vulgární jazyk 
- náš první Oscar - *Ostře sledované vlaky* (1967)
- vrcholné období - 70. léta
- nespisovnost
- **typický hrabal: hodně dlouhá souvětí**


## *Pábitel?*
- sbírka povídek
- souvisí s *Perlička na dně* - v každém z nás je něco dobrého
- co?
- pábění = zvláštní druh vyprávění, autentické rozhovory lidí, spontánní, poetická složka
- pábitel = člověk co miluje život a rád vypráví - mele a mele, nezavře pusu
- *Slavnosti sněženek*


## *Postřižiny* (1974)
- doba moderní
- zkracujou se vzdálenosti (automobil)
- všechno se zkracuje
- uřezali nohy u nábytku
- maminka si zkrátila vlasy


## *Automat svět*
- [Bohumil Hrabal - Povídky I](https://web2.mlp.cz/koweb/00/04/06/82/82/povidky_i_hrabal.pdf) (strana 15-24)

### Obsah:
- v automatu se oběsila dívka
- přijde opilý mladík, kterému utekla snoubenka, chtěla aby se zavraždili spolu
- mladík vypráví o tý svý holce
- výčepní vypráví, jak musí být u každýho maléru, když se někdo zavraždí
- zvědavci se snaží koukat dovnitř na oběšenou
- v prvním patře se svatba - pak sešli dolů a šli ven do deště
- přišli dva SNBáci, ženich jednomu z nich udělal monokl a ten ho nechal zavřít
- nevěsta nechce jít spát o svatební noci sama
- mladík se jí líbí a ona se mu líbí taky
- venku vítr ohýbá stromy a on svojí kravatou a její vlečkou je přivazuje, aby se nepolámaly
- vypráví nevěstě, jak tý svý holce dělal posmrtnou masku ze sádry, aby začala nový život, která pak nešla sundat a pak mu zpovídala a on maloval na bílou stěnu černým dehtem
- došly provazy na přivazování stromků, nevěsta nastaví ramínko, on za něj mocně vezme a strhne z ní zbytek svatebních šatů
- nevěsta tam stojí polonahá ve veřejném parku a chce aby jí taky udělal posmrtnou masku


### Charakteristika pábitele:
- **mladík** (prej tam jsou dva tak nevim)
	- předevčírem mu utekla holka
	- montér
	- pracuje ve fabrice
	- nemůže žít bez fabriky a bez svý holky
	- záplatované montérky, rozbitý škrpály, nohavice rozžužlaná vozubeným soukolím


### Zajímavá slovní spojení:
- pořád se tam opakuje *"A do automatu pronikala ze salonku v prvním patře veselá hudba a hovor, který propukal v nevázaný smích."*
- *"Proti gustu žádnej dišputát"*


### Metafory:
- *"já bych tvý mutře nejradši strčila do držky ruční granát"* i guess?
- dunno
- je tam spousta přirovnání, metafor moc ne