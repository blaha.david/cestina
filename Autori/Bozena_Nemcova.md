# Božena Němcová
- 1820? - 1862
	- *datum, původ a další okolnosti narození zůstávají nejasné*
- česká spisovatelka a významná osobnost **[národního obrození](/Literarni_smery/06_Narodni_obrozeni.md)** (čtvrtá etapa)
- pozdní romantismus, raný realismus
- nejvíce ji ovnivnila její babička Magdalena Novotná
- Ve svých deseti letech byla dána na vychování do zámečku ve Chvalkovicích, kde se seznámila s literaturou a uměním
- V 17 letech byla provdána za úředníka Josefa Němce
	- měli spolu ostré spory
	- různé názory
	- většinou na budoucnost jejich dětí (3 dcery, 1 syn)
	- rozvod
	- život v chudobě, hladu
	- smrt syna Hynka
- je pohřbena na Vyšehradě na slavíně
- **Motivy**:
	- postavení žen ve společnosti - nerovnoprávnost - emancipace
	- nerovnost ve společnosti - sociální otázka
	- rovný vztah Česka a Slovenska (pobývala ve Slovensku)


## [Babička](/Dila/Bozena_Nemcova/Babicka.md) (1855)

## Další díla
- *Divá Bára*
- **básně** - *Ženám českým*, *Moje vlast*
- **povídky** - *Dobrý člověk*, *Chudí lidé*, *Pan učitel*
- **pohádky** - *Národní báchorky a pověsti*, *V zámku a podzámčí*
- **cestopisy** - *Obrazy z okolí Domažlického*, *Z Uher*

## Další autoři této doby:

- K. H. Borovský – *Obrazy z Rus*, *Král Lávra*

- Josef Kajetán Tyl

- Karel Hynek Mácha – *Máj*, *Obrazy ze života mého*

- K. J. Erben – *Kytice*, *České pohádky*