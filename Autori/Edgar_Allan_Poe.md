# Edgar Allan Poe
- 1\. polovina 19. století (1809 - 1849)
- americký básník a esejista
- jeden ze tří dětí kočovných herců Elizabeth a Davida Poeových
- jeho otec trpěl alkoholismem a zemřel ještě před Edgarovým narozením
- matka zemřela v r. 1811 (24 let) na tuberkulózu a zanechala tři sirotky
	- malý Edgar
	- mentálně postižená Rosalie
	- Williama - stejně jako otec trpěl alkoholismem a předčasně zemřel
- ujala se ho anglická rodila Allanových - odtud pochází jeho druhé jméno
- oženil se svou čtrnáctiletou sestřenicí, o jedenáct let později zemřela
- bída a deprese, závislost na drogách a alkoholu
- zemřel v nemocnici poté co ho našli opilého v bezvědomí na chodníku
- [Romantismus](/Literarni_smery/07_Romantismus.md)

## [Havran](/Dila/Edgar_Allan_Poe/Havran.md) (1845)

## Další díla
- esej *Filosofie básnické skladby* (1947)
- *Jáma a kyvadlo*
- *Zánik domu Usherů*
- *Vraždy v ulici Morgue*


## Další autoři této doby
- Viktor Hugo
- G.G. Byron
- P. Bysshe Shelley