# Erich Maria Remarque
- **Období**: 1\. pol 20. století, 1. světová válka v literatuře (1898-1970)
- německý spisovatel
- Remarque (Remark) je pseudonym
	- pravé jméno - Erich Paul **Kramer** (pozpátku remarK)
	- styděl se být němcem
- zasažen 1. světovou válkou (po škole v 18 letech odveden na frontu)
	- zraněn
	- po návratu měl problémy se začlenit zpátky do společnosti (vystřídal spoustu povolání - účetní, automobilový závodník, učitel)
- 1931 - stěhování do Švýcarska
- po nástupu nacismu (1933) zakázaný autor - odpor k válce
- nezařazuje se do [ztracené generace](/Ztracena_generace.md) (nebyl američan, ale němec)
- 1938 - zbaven německého občanství a prohlášen za zrádce (jeho knihy páleny)
- 1939 - USA (New York)


## [*Na západní frontě klid*](/Dila/Erich_Maria_Remarque/Na_zapadni_fronte_klid.md) (1929)

## Další díla
- *Cesta zpátky* (1931) - navazuje na *Na západní frontě klid*; zabývá se problematikou opětovného začlenění do společnosti po válce
- *Jiskra života* - vypovídá o hrůzách života v koncentračním táboře
- *Poslední stanice* - divadelní hra o bombardování Berlína
- *Tři kamarádi* - tři kamarádi, veteráni 1. světové války, pracují společně v autodílně
- *Nebe nezná vyvolených* - milostný román z prostředí automobilových závodů


## Další autoři této doby
- Arnold Zweig - německý prozaik - *Válka bílých mužů*
- Romain Rolland - francouzský dramatik - *Petr a Lucie*
- Henri Barbusse - francouzský spisovatel a politik - *Oheň aneb deník bojového družstva*
- **[Ernest Hemingway](/Autori/Ernest_Hemingway.md)** - americký publicista - *Stařec a moře*