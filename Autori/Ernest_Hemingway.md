# Ernest Hemingway
- 1\. polovina 20. století (1899 - 1961)
- americká válečná próza
- čelní představitel **[ztracené generace](/Ztracena_generace.md)**
- velmi ovlivněn válečnými událostmi (těžce zraněn ve válce v Itálii)
- hrál na violoncello
- pobýval na Kubě
- Nobelova cena za dílo *Stařec a moře*

## [Stařec a moře](/Dila/Ernest_Hemingway/Starec_a_more.md)

## Další díla
- *Sbohem, armádo* - románový příběh nešťanstné milenecké dvojice, která ani po útěku z války nenachází osobní štěstí
- *Komu zvoní hrana* - vlastní zkušenosti dobrovolníka z španělské občanské války


## Další autoři ztracené generace
- [Erich Maria Remarque](/Autori/Erich_Maria_Remarque.md) - Němec - *Na západní frontě klid* 
- Francis Scott Fitzgerald - Američan - *Velký Gatsby*
- Romain Rolland - Francouz - *Petr a Lucie*
