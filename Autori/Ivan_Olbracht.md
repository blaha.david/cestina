# Ivan Olbracht (Kamil Zeman)
- 2\. polovina 19. století - 1. polovina 20. století (1882 v Semilech - 1952 v Praze)
- levicově zaměřený prozaik a novinář
- byl členem KSČ
	- několikrát vězněn na příliš revoluční názory
	- v 1. polovině 20. století odešel, protože podepsal Manifest 7
		- Manifest 7 = protest sedmi umělců proti novým názorům ve straně
- začínal jako autor ,,bosáckých“ povídek a psychologických románů
- psal do levicových novin: *Rudé právo*, *Právo lidu*, *Dělnické listy*
- v dílech se soustředí na osudy jedinců, kteří jsou výjimeční, zkoumá jejich psychiku
- [Meziválečná próza](Mezivalecna_proza.md), levicově zaměřená próza
- ovlivněn Marxismem
- vliv pobytu na Podkarpatské Rusi - mnoho motivů pro díla
- na přelomu 20. a 30. let vnitřní rozdělení levicových autorů
	- r. 1929 vyloučeno sedm spisovatelů (Neumann, Hora, Majerová, Malířová, Olbracht, Seifert, Vančura) – nesouhlasili s bolševizací po vzoru Rusů
- od 30. let hovoříme o tzv. **socialistickém realismu**
	- usiluje o zobrazení skutečností ve společenském vývoji
	- ovlivněný levicovými myšlenkami a marxismem
	- vyvíjí se vedle demokratického proudu ve 20. a 30. letech

## [Nikola Šuhaj Loupežník](/Dila/Ivan_Olbracht/Nikola_Suhaj_Loupeznik.md)

## Další díla
- *Anna Proletářka* - 1928, román, venkovanka v Praze, účastní se revoluce
- *Žalář nejtemnější* – psychologický román o chorobné žárlivosti

## Další autoři této doby
- Marie Majerová - *Siréna* – sociální román
- Marie Pujmanová - *Lidé na křižovatce, Hra s ohněm, Život proti smrti* - románová trilogie