# J. R. R. Tolkien
- celým jménem John Ronald Reuel Tolkien
- počátek až 70. léta 20. století (1892 - 1973) (narozen v Jihoafrické republice)
- [světová literatura 2. poloviny 20. století](/Literarni_smery/12_Literatura_2_poloviny_20_stoleti.md)
- anglický spisovatel
- filolog (věda o jazyku - mluvil 15 jazyky, vytvořil si své vlastní jazyky - elfština)
- univerzitní profesor anglického jazyka a literatury na Oxfordu
- byl vychováván knězem - oba rodiče mu umřeli
- jazykovědec (lingvista)
- člen skupiny *Inklings* spolu s Clive Staples Lewisem (*Letopisy Narnie*)
-  otec **fantasy** žánru
- "definice" mytologických postav - elfové, trpaslíci, skřeti, ...
- vytvořil si svůj celý promyšlený fiktivní svět
- každá reference je někde (třeba v jiné knize - Silmarillion) vysvětlena
	- každá postava, každé místo, každá událost
- inspirace z mytologie a hrdinských eposů
- hlavní motiv je souboj dobra se zlem
- motivy křesťanství
	- gandalfova smrt a znovuzrození
	- data - společenstvo prstenů vyjde z roklinky na Vánoce, Frodo zničí prsten v den Kristova ukřižování
	- monoteistický svět - jeden hlavní bůh, který troří svět
	- satan - Morgot - nepřítel
	- motivy lístosti a odpuštění
	- křesťanské hodnoty - všichni hobiti jsou stejně důležití
	- všechny postavy jsou původně dobré, ale časem se zkazí
		- Sauron byl původně anděl
		- Glum byl původně hobit
- nostalgie a smutek nad dávnými zašlými časy (byl historik)


## [Hobit aneb Cesta tam a zase zpátky](Hobit.md) (1937)


## Další díla
- *Sir Gawain a Zelený rytíř* (1924)
- *Pán prstenů* (psáno: 1937-49, vydáváno: 1954-55)
- *Silmarillion* (1977 - vydal syn Christopher)
	- spíše kronika - neklade důraz na děj a je plná jmen a letopočtů
	- odchod elfů do Středozemě, jejich královské rody, ...
	- další země (Númenor)
- *Húrinovy děti* - (2007 - vydal syn Christopher)
	- odehrává se 6500 let před pánem prstenů

*Silmarillion začal psát po Hobitovi, ale nakladatelé chtěli víc hobitů a tak ho přerušil a začal psát pána prstenů.*

## Další autoři této doby
- Ray Bradbury - USA - *Marťanská kronika*, *451 stupňů Fahrenheita*
- [E. M. Remarque](/Autori/Erich_Maria_Remarque.md) - Němec - *Na západní frontě klid*, *Cesta zpátky*
- C. S. Lewis - Anglie - *Letopisy Narnie*
- George Orwell - *Farma zvířat*, *1984*