# Jan Neruda
- největší literární osobnost 2\. poloviny 19. století - český [realismus](/Literarni_smery/08_Realismus_a_Naturalismus.md)
- narodil se: 9. července **1834**, **Malá Strana**, v domě U Dvou slunců
- zemřel: 22. srpna **1891**, na rakovinu střev (hrob na Vyšehradě)
- [**Májovci**](/Majovci.md), *(pozdní fáze národního obrození)*
- **používal trojúhelník jako podpis svých fejetonů**
	- trojúhelník = symbol plápolajícího ohně
- český básník, prozaik, novinář - **národní listy**
- na Malé Straně chodil do školy
- od mládí byl nadšen venkovem, který mu dodával osvobozující pocit
- na druhou stranu nemohl žít jinde než ve velkoměstě
- neměl žádný dlouhodobý vztah (byl samotář, uzavřený člověk)


## [Fejeton](/Sloh/Fejeton.md) = “Podčarník”
-   **zakladatel Fejetonu**
-   Žerty, hravé i dravé
	-   *1\. máj 1890*
	-   *Kam s ním?*
-   Různí lidé
-   Studie krátké a kratší
-   Menší cesty
-   Obrazy z ciziny


## [Povídky Malostranské](/Dila/Jan_Nedura/Povidky_malostranske.md)


## Další díla
-   *Hřbitovní kvítí*
	-   pesimistický, pocit beznaděje
	-   Bachovský absolutismus
	-   na 10 let se odmlčel
-   *Knihy veršů*
	-   3 knihy
	-   Dědova mísa
		-   mladí zapomínají na starší
		-   *otec dělá dědovi mísu(třesou se mu ruce a rozbíjí nádobí, syn řekl otci, že mu taky udělá takovou mísu, tím ho rozčilí)*
	-   Báseň otci
		-   Maminka  x  otec
-   *Písně kosmické*
-   *Balady a romance*



## Další autoři této doby
- [**Májovci**](/Majovci.md)