## [Jaroslav Seifert](https://cs.wikipedia.org/wiki/Jaroslav_Seifert)
 - 1901 - 1986
- jako **jediný** získal **nobelovu cenu za literaturu**
	- 1984
	- jela ji převzít jeho dcera
		- 1956 těžce onemocněl
		- byl už starý
		- finanční obnos za nobelovku vložil do nadace na financování začínajících autorů?
- [Poetismus](/Umelecke_smery/Poetismus.md)
- spojován s **Žižkovem** (kde se narodil)
	- v té době chudinská čtvrť
	- proletářská literatura
- měl zákaz publikování
	- 1948
		- převzetí moci komunistickou stranou československa
	- 50. léta - problematická
		- režim potřeboval "uklidit"
			- nepohodlní lidé - protikomunisté
		- rudá armáda
	- nenapsal nic co by bylo "v pořádku" s režimem
	- vystoupil s ostrou kritikou směřování kultury
		- hodně odvážný
		- první zákaz publikace
	- vybral si *Viktorku* z *Babičky* Boženy Němcové a napsal *Píseň o Viktorce*
		- spojuje tragický osud s tragickým osudem Boženy Němcové
	- *Maminka*
		- básnická sbírka plná něhy
		- vzpomíná na maminku (Maminčino zátiší, zrcádko, ...)
	- Nakonec podepsal Chartu 77
	- 1929 - po kritice vedení KSČ byl vyloučen


#### Motivy:
 - domov (rodina - maminka, dětství, Praha)
 - čas
 - žena (milenka, matka)

Vlastenectví
Praha - symbol češství
Božena Němcová - národní obrození, jazyk(slovní zásoba, vzdělání)
Konec života - smrt
Čas - ubíhající čas

### Období čisté lyriky
- pravidelné rýmy
- ?

