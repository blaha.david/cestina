# Jiří Orten

-   1919 - 1941
-   Vlastním jménem Jiří Ohrenstein
-   Židovského/německého původu
-   Pochází z Kutné Hory
-   Básník
-   Studoval konzervatoř v Praze
-   1939 - byl vyloučen ze studia kvůli židovskému původu
-   Podílel se na přípravě recitačních večerů a studentského divadla, vydával pod pseudonymy
-   Později si zvolil pseudonym - Jiří Orten
-   Přispíval do časopisů
-   První kniha - Čítanka jaro
-   Elegie, Ohnice, Nech slova za dveřmi
-   Zemřel na jeho 22. narozeniny při srážce s německou sanitkou
-   Některá díla vydaná posmrtně


## Tvorba
- vciťuje se do předmětů, láskyplný vztah k lidem, věcem
	- připomíná [Wokera](/Autori/Jiri_Wolker.md)
- existencialismus, pocity životní úzkosti, zoufalství, marnost, osamělost
- motivy: lásky, Boha, smrti
- pracuje s ironií


## Odpovědi na otázky:

*stáhnout text + poslech (recitace V. Preis): Sedmá elegie*
[06_sedma_elegie.mp3](https://discord.com/channels/670285727841648653/687705274555170824/931214074325389383)
[Sedmá elegie](http://web.quick.cz/asasek/orten/htm/bas6.htm)

1. **jak rozumíte obsahu**

Básník píše o svém žalu a vzpomíná na šťastné mládí, rodinu a okamžiky štěstí.

  
2. ** jaké motivy typické pro Ortena se v textu objevují**

Motivy z Bible.


3. **všimněte si metafory pádu a s čím souvisí**

Ztráta - žena, rodina, víry(boha)

  
4. **jak vysvětlíte 1. a poslední verš?**

*Píši vám, Karino, a nevím, zda jste živa,*
*píši vám, Karino, a nevím, zda jsem živ...*

V průběhu dopisu si autor uvědomí bídnost svého vlastního žití a pochybuje, zda stále žije, či jen přežívá.



*Smrt mlčí před verši* - nevztahuje se na dílo (zemřel mladý, ale jeho odkaz se zachoval)
