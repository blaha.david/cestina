# Jiří Wolker

## Balada o námořníku

Balada o námořníku  
Těžce se loučil námořník Mikuláš se svou mladou ženou. Jsou teprve tři týdny manžely a muž musí již za chlebem na moře. 
Manželka mu dává na cestu nůž, do něhož vyryla na památku srdce.
Po třech dlouhých letech se vrací Mikuláš domů. Cestou se zastavuje v Marseilli, kde v přístavní krčmě sklepnice mu připomíná slib, který jí dal před sedmi lety, že si pro ni přijde. Ale Mikuláš zapomněl na vše, vzpomíná jen na svou ženu a netuší, že odmítnutá dívka, jejíž lásku a věrnost zradil, vrhá se ze zoufalství do moře(ta dívka).
Pln touhy přichází v noci ke svému obydlí a tu osvětleným oknem vidí svou mladou ženu v milencově náručí. V záchvatu hněvu zabil psa, špatného strážce svého domu, který na něj hledí hasnoucíma bolestnýma očima jeho zrazené marseillské milenky. "Věrnou svoji lásku zabil. Nevěrnou už nezabije." nýbrž vrací se na moře a stává se hlídačem opuštěného majáku, uposlechnul kapitánovy výzvy. "Kdo ve světě jsi život vzal - ty život tady braň!"

