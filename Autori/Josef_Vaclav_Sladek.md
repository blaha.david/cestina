# 07. Josef Václav Sládek 18.09.2020

-   1845 - 1912
-   spisovatel, překladatel, novinář, učitel
-   zakladatel poezie pro děti - Sládek dětem - výchovný charakter
-   procestoval ameriku



## Díla
-   Americky orientovaná tvorba
	-   *Motivy indiánů*
-   Česky orientovaná tvorba
	-   ***České znělky***
	-   ***Selské písně***
	-   Lidovost/Vlastenecké myšlenky
-   **Překlady - Shakespeare**
	-   **33 dramat**
    
  

## Poezie pro děti

-   Vznik poezie pro děti
	-   *Zlatý máj* (Zlatý máj, ...)
	-   *Skřivánčí písně*
	-  *Zvony a zvonky* (křišťálová studánka, ...)
-   Výchovný charakter
    


## Velké, širé, rodné lány
  
==**Typický Sládek = symbol češství = Lány obilí**==

souvrať = okraj pole