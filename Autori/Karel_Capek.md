# Karel Čapek

- 1890 - 1938 (**Malé Svatoňovice** - Praha)
- rodina lékaře
- nejmladší (sestra Helena - literatura, Josef - oba starší)
- spisovatel, novinář, dramatik, překladatel
- pohřben na Vyšehradském Slavíně
- Pseudonym - Aristo, Milo, Plocek
- Filozofická fakulta UK v Praze
- Velký demokrat, ale i humanista
	- měl rád lidi
- meziválečná próza
	- [demokratický proud](/Proudy/Demokraticky_proud.md)
	- obohatil ji cestopisnými **fejetony a sloupky**
		- [fejeton](/Sloh/Fejeton.md)
		- [sloupek](/Sloh/Sloupek.md)
			- psán do sloupečku
			- vytvořil Karel Poláček
			- na rozhraní mezi článkem a fejetonem
- Zahradničení
	- kniha: *[Zahradníkův rok](https://cs.wikipedia.org/wiki/Zahradn%C3%ADk%C5%AFv_rok)*
- Rád cestoval
	- dokázal danou zemi popsat všemi smysly
	- cestoval na vlastní pšst - neovlivněn turistickým ruchem
	- navštívil místa, která jiní ne
	- **Cestopisné fejetony**:
		-  _[Italské listy](https://cs.wikipedia.org/wiki/Italsk%C3%A9_listy)_ 1923
		-   _[Anglické listy](https://cs.wikipedia.org/wiki/Anglick%C3%A9_listy)_ 1924
		-   *[Výlet do Španěl](https://cs.wikipedia.org/wiki/V%C3%BDlet_do_%C5%A0pan%C4%9Bl)* 1930
		-   _[Obrázky z Holandska](https://cs.wikipedia.org/wiki/Obr%C3%A1zky_z_Holandska)_ 1932
		-   _[Cesta na sever](https://cs.wikipedia.org/wiki/Cesta_na_sever)_ 1936
-   rozdíl mezi 20. a 30. lety
	-   v 20. letech se zabyval clovekem jako jedincem
		-   věc makropulos
			-   jeden vědec vymyslí lék na mládí a aplikuje ho na svoji dceru a ta žije 300 let
			-   vidí odcházet všechny svoje blízké
			-   vidí pořád dokola lidské chyby - hamižnost, války, ...
	-   v 30. letech se vic zameruje na kritiku problemu ve spolecnosti
-   rys Čapkovi poezie: technika ?
-   filosofie pragmatismu - praktického života
	-   pravdivé, praktické - přináší nějaký užitek
	-   **poezie relativismu:**
		-   objektivní pravda se skládá z různých pravd subjektivních
		-   povídka *Šlépěj*
			-   dva chodci jsou po cestě
			-   je napadaný sníh
			-   najednou uprostřed pole je jedna jediná šlépěj
			-   chodci vedou diskuzi jak se tam dostala
			-   oba dva mají pravdu anebo nemá pravdu ani jeden
		-   povádka *Povětron*:
			-   nehoda s letadlem
			-   pilot v nemocnici
			-   nemá u sebe doklady
			-   dohady kdo to je, co se stalo
				-   jasnovidec, jeptiška, lékař
			-   každý z nich může mít pravdu anebo ani jeden z nich
	-   u Čapka je tam vždycky Člověk



## Pátečníci
- skupina kulturních a politických osobností
- 1925
- Pátky v domě bratří Čapků
- T.G Masaryk(nebyl častý), Edvard Beneš, Ferdinand Peroutka, Eduard Bass, Karel Poláček, Jan Masaryk,...
- -> Klub sisyfos

## Domy
- Dvojdům v Praze na vinohradech
- Letní sídlo Stará huť v Dobříši

## Díla
- Začátek před 1. sv. válkou
- **ovlivněn válkou**
- Znaky
	- Rozsáhlá slovní zásoba
	- neobvyklá slova
- Rozdělení díla
	- romány zabývající se člověkem jako jedincem
	- utopické romány kritizující problémy ve společnosti
- *[Proč nejsem komunistou](/Dila/Karel_Capek/Proc_nejsem_komunistou.md)*
- *[Kniha apokryfů](/Dila/Karel_Capek/Kniha_apokryfu.md)*

### Próza:
- #### [Válka s Mloky](/Dila/Karel_Capek/Valka_s_Mloky.md)  (1935)
- #### Krakatit

### Drama:
- #### [Bílá nemoc](/Dila/Karel_Capek/Bila_nemoc.md) (1937)
- #### RUR
- #### Věc makropulos

### Hovory s T.G.Masarykem
- 3 knihy
- 1928 - 1935
- 9 let rozhovorů
- život, politické, náboženské, filosofické názory

### Dětské knihy
Dášenka, ?


# Josef Čapek
- malíř, grafik
- ilustrace
- umělecký směr: Kubismus
	- jeden z nejvýzmamnějších představitelů
- ve škole z výtvarky čtyřky
- povídání o pejskovi a kočičce - ilustrace
