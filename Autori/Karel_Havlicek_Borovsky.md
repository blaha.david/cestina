# Karel Havlíček Borovský
- 1\. polovina 19. století (1821 - 1856)
- 4\. fáze [národního obrození](/Literarni_smery/06_Narodni_obrozeni.md#4.%20fáze%20dovršení), počátky [realismu](/Literarni_smery/08_Realismus_a_Naturalismus.md)
- politicky zařazován do generace *"národních buditelů"*
- vlastním jménem *Karel Havlíček*
- přídomek "*borovský*", kterým podepisoval své články, je odvozen od jeho místa narození --> **Borová** u Přibyslavi
- velký vlastenec
- zakladatel české žurnalistiky, satiry a literární kritiky
- redaktor vládních Pražských novin --> později **Národní noviny**
	- byl první, kdo noviny psal jednoduše, aby to pochopil každý
	- dříve se psali vysokým odborným jazykem
- vstoupil do kněžského semináře
	- myslel si, že jako kněz bude mít větší slovo
	- byl vyloučen za odpor vůči církvi
- pracoval v Rusku jako vychovatel
	- obdivoval zbožnost a houževnatost práce vědců
	- zpočátku byl Ruskem nadšený
	- ale později vystřízlivěl a uviděl absolutismus, zacházení s mužiky, úroveň na venkově, negramotnost, sociální rozdíly a alkohol a vrátil se domů
- vydával časopis Slovan - jediný veřejný protivládní časopis
- 1851 deportován do Brixenu
	- v exilu napsal své tři nejznámější epické satirické básně
- zkritizoval Tylův román *Poslední Čech*
	- ve své kritice zdůraznil potřebu činného vlastenectví a zformuloval funkci literatury ve vztahu ke společnosti
- motivy:
	- hledání východiska z krize světa
	- důraz na národ, vychází z lidové tvorby a přetváří ji
	- venkovská próza, idylské dětství, sny o lepším světě
	- vlastenectví
	- hájení demokratických zásad proti vládě a církvi
- *Božena Němcová mu údajně na rakev dala trnovou korunu jako symbol mučednictví*



## [Král Lávra](/Dila/Karel_Havlicek_Borovsky/Kral_Lavra.md)

## [Tyrolské elegie](/Dila/Karel_Havlicek_Borovsky/Tyrolske_elegie.md)


## Další díla
- *Křest svatého Vladimíra* - ostré vystupování proti vládě (paroduje legendu)
- *Obrazy z Rus* - cyklus cestopisných črt
- ***Epigramy*** - pět částí - církvi, králi (vládě), vlasti, múzám, světu
	- *epigram = krátká satirická báseň, má jednu myšlenku, má výrazné zakončení(=poenta) - útočil na monarchy, policii, církev, jiné spisovatele(Tyl)*


## Další autoři této doby
- Josef Kajetán Tyl (*Strakonický dudák*, *Fidlovačka*)
- Karel Jaromír Erben (*Kytice z pověstí národních*)
- Božena Němcová (*Babička*, *V zámku a v podzámčí*)