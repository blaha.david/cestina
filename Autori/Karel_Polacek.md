# Karel Poláček
- 1\. polovina 20. století (1892-1945)
- narodil se v Rychnově nad Kněžnou - velká inspirace
- vyhodili ho z gymnázia za špatné známky a chování
- jedním z nejvýznamnějších humoristů v ČSR
	- fejetonista v Lidových novinách
- humanistická literatura, [Demokratický proud](/Proudy/Demokraticky_proud.md)
- 1944 poslán do Osvětimi
- z Osvětimi se dostal pochodem smrti do tábora Hindenburg, kde napsal poslední divadelní hru, a následně do tábora Gleiwitz, kde neprošel selekcí a byl popraven




## [Bylo nás pět](/Dila/Karel_Polacek/Bylo_nas_pet.md)

## Další díla
- *Muži v ofsajdu*
- *Židovské anekdoty*
- *Hostinec U Kamenného stolu*
- *Okresní město*