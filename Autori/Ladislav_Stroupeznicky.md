# Ladislav Stroupežnický
- 2\. polovina 19. století (1850 Cerhonice - 1892)
- neměl literární vzdělání
- při lovu málem zastřelil myslivce - pokus o sebevraždu - psychické problémy
- nejprve próza - romány a humoresky - nebyly úspěšné
- později drama - veselohry
- humorista v časopisech
- dramaturg Národního divadla


## [Naši furianti](/Dila/Ladislav_Stroupeznicky/Nasi_futianti.md)

## Další díla
-   Z Prahy a venkova
-   Po trnitých stezkách
-   Den soudu
-   Černé duše
-   Velký sen

## Další autoři tohoto období
-   **Bratři Mrštíkové** – Maryša
-   **Alois Jirásek** – Lucerna
-   **Jaroslav Vrchlický** – Noc na Karlštejně