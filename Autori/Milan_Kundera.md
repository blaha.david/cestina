# Milan Kundera
- 1929
-  1979 zbaven občanství, už se do čech nevrátil (Paříž)
- zakladatel "moderního románu" ?
- **odlišnost** milostných vztahů - chladkokrevná kalkulace
- sarkasmus, ironie, erotika
- překročení určitých hranic - žert - tragikomický závěr (vymknutí situace z rukou)
- ***Žert*** (1967)
	- román
	- ***"Optimismus je opium lidsta. Zdravý duch páchne blbostí. Ať žije Trockij!"***
- *Směšné lásky*
	- soubor povídek
		- *Falešný autostop*
			- stopování aut

