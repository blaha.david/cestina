# [Moliére](https://cs.wikipedia.org/wiki/Moli%C3%A8re)
(str. 45)

- **17. století** = 1622 - 1673
- vlastní jméno *Jean-Baptiste Poquelin* \[žan batyst poklen\]
- **francouzský dramatik**, herec, ředitel divadelní společnosti
- výborné vzdělání - studoval práva, měl být notářem
- **satirické veselohry** - zesměšňoval pokrytectví, povýšenost šlechty, církve
- lidové frašky - "nižší" hra, poukazuje na problémy společnosti
	- postavení žen ve společnosti, cynismus, lakota, přetvářka
- slovní balast (vycpávková slova), "nefunkční metafory"
- jeho jazyk měl vělký vliv na francouzštinu
- jeho postravy jsou karikatury
	- Lakomec je lakomý - žádné další vlastnosti nemá

## Období
- 17\. století
- Evropa
- boj proti šlechtě, měšťáctví a církvi
- [klasicismus](/Literarni_smery/03_Klasicismus.md)
## Dílo
- *Tartuffe* - hra proti pokrytectví, církvi, donašečství
- *Misantrop* - kritika šlechty
- [***Lakomec***](/Dila/Moliere/Lakomec.md) - komedie


## Další autoři této doby
- Francie - **Pierre Corneille** (*Cid*)
- Francie - **Jean de La Fontaine** (*Bajky*, *Eunuch*)