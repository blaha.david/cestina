# Německý píšící autoři

- Češi (většina z Prahy), ale psali texty v němčině
	- vzdělávali se v němčině
	- byli židé a nemohli se vzdělávat česky
- většinou původem židé
- nejvýznamnější německá literatura, která nevychází z území německa

* * *

- ## [Franz Kafka](/Autori/Nemecky_pisici_autori/Franz_Kafka.md)
- ## [Max Brod](/Autori/Nemecky_pisici_autori/Max_Brod.md)
- ## [Gustav Meyrink](/Autori/Nemecky_pisici_autori/Gustav_Meyrink.md)


**Pražský kruh** = sdružení německy píšících autorů
vnější a vnitřní
