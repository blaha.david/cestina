# [Egon Erwin Kisch](https://cs.wikipedia.org/wiki/Egon_Erwin_Kisch)

- pražský německy píšící reportér, investigativní žurnalista
- židovského původu
- přezdívka **Zuřivý reportér**
- touha být u všeho, pokaždé být „při tom“