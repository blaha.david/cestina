# Franz Kafka
- Neznámější [německy píšící autor](/Autori/Nemecky_pisici_autori.md)
- 20\. století  *(1883 - 1924)*
- Staré město - josefov
- pracoval jako úředník v pojišťovně - práce ho nebavila
- po většinu života těžce nemocný
- nevyrovnaný vztah s otcem - celoživotní trauma
- otec si myslel, že bude pokračovat v jeho práci - nesoulad
- špatně se čte pro nečtenáře
- žádná hlubší psychologie postav
	- někdy ani neznáme (celé) jméno hlavní postavy
- typický rys: **kritika stereotypu** byrokracie, blbost lidí
	- všichni jdou za tím, kdo je nejhlasitější
	- křičí hesla i když jim nerozumí
- svět odcizování člověka
- přízrak monstra
- pesimistický
	- úpěnlivá snaha zachránit člověka
	- nakonec se zdají všechny možnosti bezvýchodné
- Byl zvláštní i v osobním životě
	- měl strach z manželství - byl dvakrát zasnouben, ale nikdy se neoženil
	- měl syna - zemřel v 7 letech (Kafka o něm nikdy nevěděl)
- Nějakou dobu se o něm vůbec neučilo - do učebnic se dostal až v r. 1989
- Otáčecí Kafkova hlava


## *[Proměna](/Dila/Franz_Kafka/Promena.md)*
