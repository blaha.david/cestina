# Paulo Coelho
- 1947 (Rio de Janeiro)
- Brazilský spisovatel, novinář, texty písní
- Jeho touhou bylo psát, ale to mu rodiče neschvalovali a proto ho posílali do psychiatrické léčebny (elektrošoková terapie)
- V mládí patřil mezi hippie a měl problémy s drogami
- 1996 - jmenován zvláštním poradcem UNESCO pro duchovní sbližování a dialog mezi kulturami
- Vykonal tisícikilometrovou pouť do Santiaga de Compostela
	- *zážitky z ní zužitkoval ve své první úspěšné knize Poutník - Mágův deník*
- Jeho hrdinové hledají změnu životního stereotypu
- Nabádá k duchovnímu rozvoji
- V březnu 2016 zavítal do Prahy

## Zařazení autora
- současná světová literatura
- latinskoamerická literatura:
	- vrcholné období v 60. letech 20. století
	- magický realismus
		- realita se prolíná s iluzí
		- nelogické jevy jsou přijímány přirozeně, bez vysvětlení


## [Alchymista](/Dila/Paulo_Coelho/Alchymista.md)

## Další díla
- *Poutník – Mágův deník* – prvotina, vychází z osobního zážitku cesty do Santiaga do Compostela
- *Veronika se rozhodla zemřít* – román, první díl trilogie A dne sedmého...
- *Jedenáct minut* – román, životopis prostitutky Márie

## Další autoři této doby:
- Michail Bulgaok *(Mistr a Markétka)*
- Günter Grass *(Plechový bubínek)*
- Jiří Kratochvíl *(Uprostřed noci zpěv)*
- Gabriel G. Márquez *(Kronika ohlášení smrti)*