# Václav Hrabě
- Skupina *Beat generation* (hnutí v americe v 60. letech)
- 1940 - 1965
- gymnázium **Hořovice** (čeština, dějepis)
- pražská ***Reduta*** (jazz)
- jeho básně
	- milostná lyrika
	- úpřímná výpověď citů a názorů mladého člověka
	- touha po svobodě x politické deformaci
	- udehrávají se v Praze (Petřín)
- úmrtí na otravu plynem (25 let)
	- nešťastná náhoda??
	- sebevražda??
- typický Hrabě:
	- spát
	- láska, má strach aby to nepokazil
	- nepravidelný rým
	- střet generací


## Tvorba
- sbírka *Blues pro bláznivou holku* (1991)
	- *Variace na renesanční téma*
		- zhudebněno skupinou *Etc* (Vladimír Mišík)
		- motiv: láska - světlá, žhnoucí, pomíjivá, nevybírá si, nestálá, bolestná, krutá
		- básník oslovuje čtenáře
- *Horečka* (próza)
	- jediná dochovaná povídka
	- jeden den v životě osmnáctiletého mladíka - cigarety, alkohol, trable s láskou
	- náhled do jeho duše


