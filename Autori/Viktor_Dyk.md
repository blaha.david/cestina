# Viktor Dyk
- přelom 19. a 20. století (1877 - 1931)
- generace **[anarchystických buřičů](Anarchisticti_burici.md)**
	- odpor k společnosti
	- navazuje na českou modernu
- redaktor Národních listů
- vášnivý a uznávaný šachista

## [Krysař](/Dila/Viktor_Dyk/Krysar.md) (1915)

## Další díla
- *Buřiči* - veršované povídky, vrcholí protispolečenský postoj, blíží se k anarchismu, motiv touhy po činu a následného zklamání
- *Satiry a sarkasmy* - sbírka poukazuje na nedostatky české povahy
- *Pohádky z naší vesnice* - politická a kultirní satira
- *Zmoudření Dona Quijota* - drama


## Další autoři této doby
- **Fráňa Šrámek** - *Života bído, přec tě mám rád* (osudy vyděděnců)
- **František Gellner** - *Po nás ať přijde potopa, Radosti života* (ironický, výsměšný postoj vůči společnosti; cynismus)
- **Karel Toman** - *Pohádky krve* (básnická sbírka; symbolismus, dekadence)