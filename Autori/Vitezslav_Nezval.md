## Vítězslav Nezval
- Zakladatel [Poetismu](/Umelecke_smery/Poetismus.md)
- příklon k Surrealismus
- 50 hořkých balad
	- Robert
- Básnická sbírka *Matka Naděje*
	- 1938
		- hrozba fašismu
		- má tušení, že to nedopadne dobře
	- onemocnila jeho matka
	- Naděje, že válka skončí dobře


 
### Manon Lescaut - Vítězslav Nezval
 Manon - 17 let
 poslaná rodiči do kláštera
 na cestě potká rytíře des Grieux
 zamilují se
 odstěhují se do Paříže
, ale nemají prostředky
 Duval - Soudní prokurátor
 Duval se jí dvoří, manon se mu líbí
 Manon ho představí jako bratra
 nakonec Manon a des Grieux jsou vyhoštěni od Ameriky
 končí to tím, že Manon na lodi umírá.
 
 z hlediska **modality** = z postoje mluvčího
 
 přísudek jmenný se sponou:
 *být + přídavné jméno*
 podvedený - složený tvar
 podveden - jmenný tvar
 
 
