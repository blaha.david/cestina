# Vladislav Vančura
- v dětství nestálé zázemí
	- 7 stěhování, výměny škol
- Malostranské gymnázium
- protirakouské myšlenky
- kázeňské postihy
- inspirace k napsání knihy *Benešova*
	- popisuje zážitky ze školy
- Lékařská fakulta
- Vančurova vila na Zbraslavi
	- sám si ji postavil (byl i architekt)
- Devětsil
- avantgardní umělci, [poetismus](/Umelecke_smery/Poetismus.md)
- vyloučen z KSČ
- 1942 zatčen gestapem a popraven na Kobyliské střelnici


## Dílo
### Rozmarné léto
- jeho nejznámější dílo
- život narušen příjezdem kouzelníka Arnoštka
- svádí ženu
- 


## Vančura u filmu
- *Nenapravitelný Tommy*
- *Ival Olbracht*
- *Bohuslav Martinů*
- *Vítězslav Nezval*