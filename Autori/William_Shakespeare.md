# [William Shakespeare](https://cs.wikipedia.org/wiki/William_Shakespeare)
(str. 31)

- 16\. - 17. století (1564 - 1616)
-  největší anglický i světový dramatik
-  The Globe Threatrewwe
-  [Renesance](/Literarni_smery/01_Renesance.md)
-  jeho dramatickou tvorbu do češtiny přeložil [J.V.Sládek](/Autori/Josef_Vaclav_Sladek.md)


## Období
- plně [renesanční](/Literarni_smery/01_Renesance.md) autor
- jeho dílo je zcela světské
- hrdinové jsou sami strůjci svých osudů
- jejich činy vyplívají z jejich charakterů
- často rozporné, živoucí postavy


## Dílo
- Do r. 1600
	- komedie, hry z anglických dějin a první tragédie
	- *Zkrocení zné ženy*, *Sen noci svatojánské*
	- *[Julius Caesar](https://cs.wikipedia.org/wiki/Julius_Caesar_(Shakespeare) "Julius Caesar (Shakespeare)")*, *Král Jan*, *Titus Andronikus*
	- *[Romeo a Julie](/Dila/William_Shakespeare/Romeo_a_Julie.md)*
- období 1601 - 1608
	- velké a slavné tragédie
	- *Hamlet*, *Othello*
- po r. 1608
	- vyjasnění, naděje
	- romantické hry
	- *Bouře*
	- *Sonety* - odrazem jeho milostných a přátelských citů