# Zdeněk Jirotka
- 20\. století (1911-2003)
- narozen v Ostravě
- nedostudoval reálku, vyučil se zedníkem
- autor humoristické literatury (fejetonista)
- podílel se na televizním a rozhlasovém pořadu *Sedmilháři*
- Lidové noviny
- okupace, protektorát, heydrichiáda (atentát v květnu 1942)
	- díla bez politiky

## Literárně historické pozadí
- 2\. pol\. 20\. století - **humoristická literatura**
- kritika a zasměšnění záporných lidských vlastností
- další autoři této doby:
	- **Vladislav Vančura** - *Rozmarné léto*
	- [**Karel Poláček**](/Autori/Karel_Polacek.md) - *Bylo nás pět*
	- **Eduard Bass** - *Cirkus Humberto*, *Klapzubova jedenáctka*
	- [**Karel Čapek**](/Autori/Karel_Capek.md) - *Povídky z jedné kapsy*, *Povídky z druhé kapsy*
	- Jaroslav Žák


## [Saturnin](/Dila/Zdenek_Jirotka/Saturnin.md)


## Další díla:
- ***Muž se psem*** - humorná parodie na detektivní román o hloupých podvodnících a neomylném amatérském detektivovi, který odhalí zločince
- ***Profesor biologie na žebříku*** - sbírka anekdot a kratších povídek z let 1951 až 1955; s humorným nadhledem si všímají záporných jevů naší současnosti
- ***Sedmilháři***