# [Antoine de Saint-Exupéry](/Autori/Antoine_de_Saint-Exupery.md)

## Malý princ
- **filozofická pohádka**
- autorovi ilustrace
- **motivy:**
	- ztroskotání pilota uprostřed pouště
	- zamyšlení nad pravými hodnotami lidského života
	- otázky morálky, dobra, zla, přátelství
	- střet dětského světa se světem dospělých
	- zodpovědnost v konfrontaci dětského a dospělého pohledu na svět
		- nepohopení dětí dospělými
		- neschopnost dospělých chovat se dětinsky
- **hlavní myšlenka**: Každý z nás má v sobě malého prince, přestože jsme již dospělí. Často se chováme jako děti.
- Chronologický děj se odehrává na poušti.
- Retrospektivní děj na planetkách, odkud malý princ přicestoval.
- spisovný jazyk, dialogy prince s ostatními postavami
- homodiegetický vypravěč = ich-forma - pilot, er-forma - malý princ vypráví
- kniha je určena dospělým lidem, aby nemysleli pouze na sebe, ale i na ostatní a hlavně na děti.


### Charakteristika postav
- #### *Malý princ*
	- malé postavy
	- zvědavý
	- nikdy neodpoví na otázku, vždy trvá na své otázce
	- starostlivý (květina)
- #### *Pilot*
	- ztroskotal s letadlem v poušti
	- zpodobnění s autorem
	- vypravěč
- #### *Květina*
	- je na planetce, odkud pochází princ
	- stará se o ni
	- byla domýšlivá, marnivá
	- ale před odletem princi sdělila, že ho má ráda
- #### *Další postavy*:
	- *Karikatury lidských vlastností*
	- **Král** - panovačný, myslí si jak je rozumný. Potřebuje neustále dokazovat svou autoritu, ale nemá komu vládnout
	- **Domýšlivec** - je samolibý, slyší jenom chválu, žije na planetě sám
	- **Pijan** - nešťastný a osamělý člověk, který neustále pije, proto, aby zapomněl, že se stydí, za to, že pije
	- **Byznysmen** - myslí si, že je majitelem hvězd, neboť nikoho jiného před nim nenapadlo, že by mu mohly patřit, neustále je přepočítává
	- **Lampář** - pracovitý, věrný příkazu, ale smutný z toho, že nemá čas na tolik oblíbený spánek - *Sisyfovská práce*
	- **Zeměpisec** - nikdy neopouští svůj stůl, přijímá pouze badatele. Když byl princ na této planetě, zeměpisec mu řekl, že květiny jsou pomíjející, a princi bylo líto, že svou květinu nechal samotnou. Zeptal se zeměpisce, kterou planetu by měl navštívit dál, a on mu doporučil Zemi.
	- **Vyhybkář** - člověk z planety Země, postižen hektickým životem lidí
	- **Obchodník** - člověk z planety Země, prodává pilulku na žízeň, aby člověk ušetřil čas tím, že nebude muset pít
	- **Liška** - stane se kamarádkou prince, vysvětluje mu pojem ochočit – **zamilovat se, přátelství, ztráta svobody, přizpůsobení**
	- **Had** - lstivý, mluví v hádankách
	


### Obsah
- Tato slavná kniha vypráví o pilotovi (autorovi), který ztroskotal na Sahaře. Tam se setkává s malým princem, který přiletěl ze své planetky ve vesmíru na Zemi, protože si nebyl jist láskou své květiny, kterou miloval. Pomůže pilotovi najít studnu s vodou a po hadím uštknutí se vrací na svou planetku. Je to filozofická pohádka, ke které se mnohdy vrací děti i dospělí.
- Při návštěvě první planety se setkává malý princ s králem, který vidí svět velice zjednodušen, nesnesl odpor proto dával rozumné rozkazy.
- Na další planetě  potkává domýšlivce, jenž slyšel pouze chválu.
- Dále poznává pijana na třetím asteroidu, který pije, protože se stydí za to že pije.
- Čtvrté planetě vládne byznysmen, kterému patří všechny hvězdy, a tak je pořád přepočítává. Přivlastnil si je, protože nikdy ikoho před ním nenapadlo, že by mohli někomu patřit.
- Pátou planetu obývá lampář a vítilna, která je ze všech nejmenší.
- Šestá planeta byla 10x větší a bydlil na ní starý pán zeměpisec) a spisoval obrovské knihy.
- Sedmou a také poslední planetou se mu stala planeta
- Země, kde se seznámil s liškou která ho naučila co je to tajemství čistého srdce,
dobro a krása.  
