# [Božena Němcová](/Autori/Bozena_Nemcova.md)

## Babička (1855)
- povídka (novela? - sporné)
- obrazy z venkovského života
- **Jazyk díla**:
	- zastaralá čeština, archaismy (zastaralé), historismy (zaniklé)
	- lidové výrazy, dlouhá souvětí, rozsáhlé popisy osob a prostředí (až přehnaně moc popisu)
	- hodně přechodníků
- **Kompozice**: kapitoly, které na sebe příliš nenavazují
- **Tématika**:
	- idealizace českého venkova a důraz na české zvyky a obyčeje
	- zidealizovaná babička se snaží všem pomáhat a radit
	- chvála dobrých lidských vlastností
- dílo je částečně autobiografické, babička napsána dle vzoru její babičky Magdaleny Novotné, autorka se potom sama stylizuje do postavy Barunky
	- retrospektiva - vracení ve vzpomínkách na skutečné události
- filmové zpracování režiséra Antonína Moskalyka z roku 1971


### Charakteristika postav
- #### *Babička*
	- moudrá, laskavá
	- dbá na obyčeje a náboženské tradice
- #### *Terezka*
	- babiččina dcera - matka dětí
	- nemá moc ráda pověry a báchorky
- #### *Pan Prošek*
	- Tezezčin manžel - otec dětí
- #### *Barunka*
	- nejstarší z dětí
	- babiččina oblíbenkyně
	- ráda naslouchá babičce a učí se jejím zvykům
- #### *Adélka*
	- nejmladší sestra Barunky
	- zvědavá, bystrá, učenlivá
- #### *Jan a Vilém*
	- Barunčini zlobiví bratři
- #### *Paní Kněžna*
	- inspirována hraběnkou Eleonorou z Kounic
	- v jejích službách sloužil pan Prošek (otec dětí, zeť babičky)
	- milá, obětavá
	- ráda naslouchá vyprávění babičky, dá na její slova
- #### *Komtesa Hortenzie*
	- chráněnka kněžny
	- milá
- #### *Viktorka*
	- dívka, která se zamiluje do vojáka
	- když otěhotní, voják jí opustí
	- zblázní se, začne žít sama v lese
	- své dítě hodí do jezu
	- při jedné bouřce ji zabije blesk
	- *Němcová tímto příběhem chtěla ukázat na nespravedlivost vůči ženám, které se rozhodnou převzít život do vlastních rukou a společnost je za to odsuzuje.*
- #### *Kristla*
- #### *Míla*



### Obsah
Kniha začíná příjezdem babičky na Staré bělidlo, kam se vydává za svou dcerou, Terezou Proškovou, a za vnoučaty – Barunkou, Adélkou, Janem a Vilímem. Všichni si babičku okamžitě zamilují (její dobré srdce vycítí i psi Sultán s Tyrlem ). Babička se dětem stává vychovatelkou, chůvou i učitelkou. Vypráví jim pohádky, učí je lidovým zvykům a lásce k vlasti, práci a ke všemu živému. Část knihy je věnována právě popisu lidových zvyků, příběhů a pověstí (Vánoce, Velikonoce, Mikuláš, Dožínky, vyhánění zimy…)

Postupně si svou vlídností získává i všechny sousedy, kteří za ní často chodí „pro radu“. Nejčastěji ji navštěvují mlynář a myslivec s rodinami. Radostně ji přivítají i na zámku, kde sloužil otec dětí (většinu roku byl však ve Vídni), pan Prošek, a kam na jaro přijížděla komtesa se slečnou Hortenzií. Té babička pomohla ke sňatku s učitelem malířství, a ke štěstí přispěla také Mílovi a Kristle, kteří se měli brát, ale Míla byl povolán na vojnu. Proto se babička u kněžny přimlouvá za jeho návrat.

Část knihy se také věnuje vyprávění životních příběhu hl. postav. Babička si totiž pamatuje mnoho osudů venkovských lidí, z nichž snad nejvíce dojímá baladický příběh bláznivé Viktorky. Když babička zemřela, lidé na Starém bělidle si nedovedli představit život bez té laskavé, dobrotivé a moudré babičky a proto nebylo snad jediného člověka, který by její smrt neoplakal a který by ji neměl aspoň trochu rád. Pohřebním průvodem a slovy hraběnky Zaháňské „ Šťastná to žena!“ Tím končí kniha.