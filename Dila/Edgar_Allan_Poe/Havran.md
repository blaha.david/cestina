# [Edgar Allan Poe](/Autori/Edgar_Allan_Poe.md)

## Havran (1845)
- lyrickoepická baladická báseň
- vyjadřuje úzkost a hrůzu ze života i smrti
- pocit strachu a beznaděje – každý život končí smrtí a ani láska na tom nic nezmění
- kontrast (život – smrt, černý havran – bílá busta, bouřka venku – klid v pokoji)
- monotónní refrén - "nevermore" ("víkrát ne", "nikdy víc")
- mnoho českých překladů (Jaroslav Vrchlický, Vítězslav Nezval)
- hororová atmosféra
- havran je symbolem smrti a beznaděje
- velmi promyšlená vysoustružená báseň
- ABCBBB + vnitřní rým, 4. a 5. epifora, 6. verš je poloviční, aliterace

## Postavy

**básník/milenec** – **sám autor** – těžce nese smrt své milé Lenory a není schopen přestat na Lenoru myslet – rozptýlení hledá ve vědeckých spisech, jeho myšlenky a vzpomínky ho ale stejně vždy odvedou od četby a studia – rozhovor s Havranem ho ničí, snaží se ho vyhnat, ten ale se vyhnat nenechá – v závěru básník metaforicky konstatuje, že havran už nikdy neodletí z jeho duše

**Havran** – **posel/prorok** vyšší síly (básník ho nazývá poslem z pekel) – nazývá ho tak však, kvůli slovům co Havran přináší – znepokojuje muže svou záhadností – na všechny otázky odpovídá jedním slovem – „nevermore“ – sděluje básníkovi fakta a nutí jej si je uvědomit, ať už ho to sebevíc bolí – je bezcitný a neoblomný, symbol zla 

**Lenora** – na scéně se nevyskytuje, ale celý příběh se točí kolem ní

## Obsah
- Děj se odehrává v temné noci v pokoji lyrického subjektu
- v originále v prosinci, v překladu v lednu

Jedné zimní noci sedí básník nad knihami, v nichž hledá zapomnění na zemřelou lásku Lenoru. Znenadání se ozve zaklepání na okno, básník okno otevře a do pokoje vletí černý havran, který se usadí na bílé bustě Pallas Athény a s přísným zamračeným pohledem pozoruje básníka. Básník po chvíli s Havranem začne rozmlouvat, vidím v něm jakéhosi proroka a tak se ho ptá na různé otázky, například zda po smrti nalezne vytoužený klid nebo zda ještě někdy spatří Lenoru. Havran mu však na všechny otázky odpovídá „nevermore“ – víckrát ne.