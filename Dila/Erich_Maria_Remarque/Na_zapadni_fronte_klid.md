# [Erich Maria Remarque](/Autori/Erich_Maria_Remarque.md)


## *Na západní frontě klid* (1929)
- válečný historický román
- protiválečná kniha
- **motivy**:
	- ukazuje hrůzy války (utrpení, bída, ztráta)
	- problémy s opětovným začleněním do společnosti po válce
	- ztráta ideálů
	- odtržení od normálního života
- autobiografické prvky - jeho vzpomínky na válku
- úvahové prvky
- převažují vypravěčovy monology, dialogy mezi vojáky
- **jazyk**:
	- převážně spisovný jazyk
	- hovorové výrazy (brajgl, palice, švindl)
	- vojenský slang
- ich-forma, na konci er-forma
- **umělecké směry**: naturalismus, symbolismus a existencialismus
- **hlavní myšlenka**:
	- Ukázat skutečnou podobu a nesmyslnost války.
	- Při válce se lidí zabíjí, aniž by někdo z nich věděl, proč.
	- Nikdo nemá důvod zabíjet druhého člověka, ale přesto musí.


### Charakteristika postav
- #### *Paul Bäumer*
	- vypravěč (Remarque)
	- 18 letý student
	- statečný, obětavý
	- zpočátku plný odhodlání a ideálů
	- těší se na frontu, později zažívá rozčarování, zklamání z válečných hrůz, psychicky se hroutí (stane se z něj cynik)
- #### *Himmelstoss*
	- velitel výcviku
	- krutý, bezohledný - šikana
- #### *Stanislav Katczinsky*
	- velitel roty
	- zkušený - velká opora pro vojáky
	- spřátelí se s Paulem
	- "jediný nepřítel je smrt (ne francouzi)"
- #### *Kantorek*
	- třídní učitel a tělocvikář
	- nadšený vlastenec
	- přesvědčil je, aby dobrovolně šli na frontu
	- sám je ale pokrytec a zbabělec a na frontu nejde
- #### *Albert Kropp, Müller, Leer, František Kemmerich, Tjaden*
	- Paulovi spolužáci


### Obsah
- Děj se odehrává během 1. světové války na zápatní frontě Německa s Francií
- Po vypuknutí první světové války se Pavel se svými spolužáky z gymnázia rozhodne vstoupit do armády.
- První boje na frontě všem otevřou oči a ukážou válku v pravém světle. Skoro polovina roty byla už zabita, vojáci, kteří přežijí, dostávají dvojité porce jídla a tabáku. Roty jsou doplňovány mladými chlapci bez výcviku a Pavel postupně ztrácí všechny své kamarády. Jako první zemře Kemmerich po amputaci nohy. Pro Pavla je velkou oporou velitel roty Stanislav Katczinsky.
- Pavel dostává dovolenou a odjíždí domů, kde zjistí, že jeho matka umírá na rakovinu. Zároveň zjišťuje, že už ho nezajímají jeho dřívější záliby a s lidmi z města si už nerozumí a začíná se psychicky hroutit.
- Pavel je zpět na frontě a při průzkumné hlídce se schová do kráteru od granátu, kam také skočí jeden Francouz (Duval), kterého Pavel zabije, později toho však lituje.
- Pavel se proplíží zpět na frontu a umírá mu další jeho přítel Müller na průstřel břicha. Poté rota dostává rozkaz, že bude hlídat vyklizenou vesnici se skladem jídla, kde si odpočinou. Jsou ale přepadeni dělostřeleckou palbou a Pavel s Albertem jsou zraněni a převezeny do nemocnice, kde Albert po amputaci nohy umírá.
- Je rok 1918 a na frontě je očekáván útok. Katczinsky je postřelen do nohy a Pavel ho na ramenou odnese do nemocnice, kde ale zjišťuje, že je mrtvý, protože po cestě dostal zásah střepinou do hlavy.
- Pavel umírá v říjnu 1918, v tichý a klidný den, že zpráva vrchního velitelství podala pouze větu:
- *Na západní frontě klid.*