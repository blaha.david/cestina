# [Ernest Hemingway](/Autori/Ernest_Hemingway.md)

## Stařec a moře
- **novela**
- **motivy**:
	- lidské myšlení a chování
	- koloběh lidského života
	- prostý člověk, který pro dosažení cíle udělá vše
	- opovrhování lidmi, kterým se nedaří
	- závist, lidská hrdost a víra
	- neustálý boj člověka s přírodou
	- lidskou statečnost a velkou duševní sílu
- er-forma (vypravěč pouze konstatuje fakta, nic nevysvětluje)
- ich-forma (rozmluvy Santiaga s chlapcem, s rybou a se sebou samým)


### Charakteristika postav
- #### *stařec Santiago*
	- velmi pevná vůle
	- skromný
	- musí tvrdě pracovat, aby se uživil
- #### *chlapec Manolin*
	- obětavý, laskavý
	- má starce rád - pomáhá mu


### Obsah
Děj se odehrává v malé rybářské vesnici na Kubě poblíž hl. m. Havany ve 40. letech 20. století a na moři

Kniha vypráví o starém rybáři Santiagovi, pro kterého je rybaření jedinou obživou. Starci se ovšem dlouho nedaří chytit větší rybu, a proto je ostatním rybářům k smíchu. Aby toho nebylo málo, tak s ním na moře, kvůli zákazu svého otce, přestane jezdit velký pomocník, chlapec Manolin. 

Stařec má ovšem svoji hrdost a rozhodne se všem dokázat, že stále dokáže chytit velkou rybu. Druhý den ráno se proto na moři vydá dál, než kdy byl. Kvůli svým zkušenostem ví, že pokud chce chytit velkou rybu, tak musí udice spustit do velkých hloubek. 

Za nějakou dobu zabere opravdu velká ryba. Jenomže má spoustu sil a stařec ji proto nemůže ulovit. Zápasí s ní dva dny bez spaní a s drobnými zraněními. Rybář je ovšem rozhodnut, že nepovolí a rybu zabije, nebo že zabije ryba jeho. Po dvou probdělých nocích ryba ztrácí sílu a Santiago ji uloví. Zjistí, že je to mečoun a je několikrát větší než loďka. 

Stařec ulovenou rybu přiváže k loďce a vydá se domů. Jenže krvácející ryba přivábí několik žraloků. Rybář se s nimi snaží bojovat, ale nemá šanci a žraloci mu rybu do posledního drobečku sežerou. Zůstane jenom kostra, se kterou se v noci vrátí do přístavu a vyčerpán odejde do svého domu. Ráno ho v posteli objeví chlapec, počká, až se probudí, a potom mu povídá, kolik lidí se shromáždilo kolem obří kostry. Lidé kostru ryby obdivují a on na sebe může být hrdý.