# [Franz Kafka](/Autori/Nemecky_pisici_autori/Franz_Kafka.md)
## Proměna
- nevysvětlené postavy
- není popis
- vnitřní popis "brouka"
- postavy s ním interagovali
- Hlavní postava - Řehoř Samsa
- Děj:
	- jednou se probudí jako brouk
	- nemá strach z toho, že je z něho brouk
	- má strach z toho, jak se dostane do práce
	- rodina:
		- matka ho lituje, ale později se její přístup mění na "ať zaleze co nejhlouběji, ať nám neudělá ostudu"
		- otec 
		- jediná sestra k němu potom chodí a snaží se mu pomoct
	- co tím autor myslel: neproměnil se v brouka, ale mohl třeba vážně onemocnět, nebo měl nějaký úraz, což mu znemožnilo odejít do práce
	- Rozbory:
		-  https://rozborknihy.cz/promena-rozbor-knihy/
		- https://www.atlaso.cz/franz-kafka-promena-rozbor/