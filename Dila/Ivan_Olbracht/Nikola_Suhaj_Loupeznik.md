# [Ivan Olbracht](/Autori/Ivan_Olbracht.md) (Kamil Zeman)

## Nikola Šuhaj Loupežník
- románová balada - baladický román
- neřadíme ho k levicově zaměřené próze (není politicky zaměřené)
- založeno na skutečné události, autor se snaží vykreslit životní podmínky v Podkarpatské Ukrajině, která byla nejchudší oblastí naší republiky
- děj s odehrává ve vesnici Koločava v Zakarpatské Ukrajině (nejchudší oblasti), místy v horách, na konci světové války a v prvních poválečných letech.
- **smysl díla**:
	- oslava Šuhaje a jeho vlastností, které by měly lidé mít
	- nebojácnost; zájem o to, aby se měl dobře celý lid
	- miloval jednu ženu a nepodváděl ji
	- bohatým bral a chudým dával
- **kompozice**: postup chronologický, obsahuje 6 povídek (Koliba nad Holatýnem, Koločava, Nikola Šuhaj, Oleksa Dovbuš, Eržika, Přátelé
- **tematicky podobné dílo**: Alexander Dumas - *Robin Hood* - také bohatým bral a chudým dával



### Charakteristika postav
- #### *Nikola Šuhaj*
	- statečný, silný, spravedlivý, chytrý
	- miluje Eržiku
	- chce žít normální život
	- nechce válku
	- díky lektvaru ho nemůže zranit žádná kulka
	- Nikolovi “přátelé“- Juraj Drač, Adam Chrepta, Vasyl Derbak Derbaček, Ihnat Sopek, Danyl Jasinko
- #### *Eržika*
	- miluje Nikolu, ale podvede ho s četníkem
	- zachrání Nikolovi život (obětuje se pro něj - nechá se zajmout)
	- statečná, chytrá, veselá
- #### *Jura*
	- Nikolův mladší bratr
	- oddaný Nikolovi
	- nemá rád Eržiku - bojí se, že kvůli ní Nikola zemře
	- unáhlený
- #### *Němec*
	- Nikolův kamarád z války
	- také ho nemůže zranit kulka
- #### *Baba*
	- ruská čarodějnice
	- dá Nikolovi a Němcovi vypít lektvar proti kulkám, aby nezemřeli a mohli se oženic s jejíma dcerama
	- znetvořená - má ocas
- #### *Abram Beer*
	- žid
	- bohatý obchodník
	- proradný, hamižný, zlý, lichvář, šidí lidi
	- nenávidí Nikolu
- #### *Juraj Drač*
	- bratr Eržiky
	- miluje svojí sestru
	- nenávidí Nikolu
- #### *Ivan Drač*
	- otec Eržiky
	- nemá rád Nikolu
- #### *Lenard Béla*
	- četník
	- nenávidí Nikolu, chce ho chytit
	- později zešílí
- #### *Herš Wolf*
	- starosta
	- bohatý, hamižný, okrádá lidi
	- nenávidí Nikolu
- #### *Vasyl Derbak*
	- lidem zvolený starosta
	- prudký, nerozvážný, tvrdý
- #### *Svozil*
	- četník
	- miluje Eržiku
	- nerozvážný, nešťastný (Eržika se obrátí k Nikolovi)


### Obsah
Je první světová válka a z boje utíkají dva muži. Bydlí u staré baby, která jim přičaruje moc, že nemohou být zraněni střelnými zbraněmi. Poté se muži opět vrací do války a vyzkouší si svoji nezranitelnost.

Nikola se rozhodne opět vrátit domů do Koločavy, kde se ožení s Eržikou. Poté je chycen a vrácen zpět na frontu. Eržika utíká do lesů. Pouze obchodníci jsou bohatí a tak je Nikola přepadává a nemá z ničeho strach a je oslavován lidem. Nikola se schovává, válka mezitím skončila a je znám mezi lidmi kvůli své zlodějské činnosti. Nikdy se neměl místní lid lépe, ale četnictvo chce Nikolu zadržet.

Ve vesnici vznikají konflikty. Poté zradí Nikolu dva kamarádi, protože na něj byla vyhlášena odměna třicet tisíc za jeho hlavu. Díky Eržice, která ho varovala, se nic nestalo. Nakonec ale Nikolu a jeho bratra Juraje ubijí dva kamarádi sekerou, protože by rádi získali finanční odměnu. Vrazi se chlubí četníkům, ale četníci jim vysvětlí, že získat odměnu je velice složité a dopadení také vezmou jako svoji zásluhu. Lid pláče, že je Nikola Šuhaj – jejich hrdina, z něhož se stala legenda - mrtev.

Po Nikolově smrti se narodí malá Anča, dcera Nikoly a Eržiky.