# [J. R. R. Tolkien](Autori/J_R_R_Tolkien.md)

## Hobit aneb Cesta tam a zase zpátky
- fantasy román
- motiv - boj dobra se zlem
- prsten - metafora závislosti (nástroj zkázy)
- er-forma
- vševědoucí vypravěč
- spisovná čeština
- vyprávění, popis, charakteristika, úvaha
- přímá řeč


### Charakteristika postav

- #### *Bilbo Pytlík*:
	-   hobit
	-   má rád svůj klid, pořádek, dobré jídlo a lenošení - netouží po dobrodružství
	-   v průběhu knihy se mění
	-   přestalo mu záležet na pořádku kolem něj
	-   na konci knihy Bilbovi bylo jedno, že se s ním ostatní hobiti nebavili
    
- #### *Gandalf*:
	-   čaroděj
	-   Poskytl trpasličí artefakty - mapu a klíč k Osamělé hoře.
	-   Přemluvil Thorina aby vzali "zkušeného zloděje" - Bilba.
	-   Dokázal si najít přátele mezi lidmi, hobity, elfy i trpaslíky - mají ho rádi kvůli dobrosrdečnosti, obětavosti a ochotě pomáhat druhým. Vždy stojí na straně dobra a je ochoten za něj bojovat.
    
- #### *Thorin Pavéza*:
	-   vůdce a král trpaslíků
	-   moudrý, odvážný, odhodlaný
	-   touží získat zpět poklad patřící jeho předkům
	-   vidina bohatství jej ale zaslepí z skoro zešílí
    
- #### *Elrond*:
	-   král elfů v Temném hvozdu
	-   ušlechtilý, silný, moudrý, úctyhodný
    
- #### *Medděd:
	-   člověk se schopností měnit se v medvěda
	-   nenávidí skřety, silný a mocný
	-   rozumí si se zvířaty
    
- #### *drak Šmak*:
	-   rudozlatý drak, který vyhnal trpaslíky (Thorinovy předky) ze Železných hor
	-   zuřivý, uřážlivý, chamtivý
    
- #### *Bard*:
	-   člověk z jezerního města
	-   statečný a čestný
	-   schopný učištník
	-   po útoku Šmaka se stal vůdcem Jezerního města
	-   zabil Šmaka černým šípem

- #### *Glum*:
	-   původně hobit
	-   pohltila ho moc prstenu