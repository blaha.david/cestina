# [Jan Neruda](Jan_Neruda.md)

## Povídky Malostranské
- plošně vyšly 1878 (samostatně už dřív v časopisech)
- soubor 13 povídek
- několik povídek er-formou, několik ich-formou
- odehrávají se v Praze na Malé Straně (všechny jsou spojené prostorem)
- zobrazení života na Malé Straně
- důraz na obyčejné lidské vlastnosti
- každá povídka má jednu hlavní postavu
- občas se míchají postavy z jiných povídek
- nejsou optimistické - tragické konce, bez hrdinství
- "figurky"
	- karikatury
	- jedna vlastnost (pomlouvání, )
	- maloměsto
	- odráží temné stránky národní morálky


### Obsah
V knize se vyskytují povídky různého druhu. V některých zachycuje Neruda typické malostranské povídky, například v povídce ***Hastrman***, kde vypráví o panu Rybáři, kterého přezdívají hastrman. Vyprávělo se o něm, že je bohatý, protože vlastní sbírku drahokamů. Ve skutečnosti to byly ale obyčejné kamínky. Takto se postupně odhaluje pravda, jako třeba v povídce ***U tří lilií***. V ní se objevuje neobyčejně krásná dívka s podmanivýma očima. Později se však ukáže, že je bezcitná.

V jiných povídkách, jako třeba ***Svatováclavské mše***, se vyskytují příběhy z vlastního života. Autor tu vystupuje jako malý chlapec, který chce strávit noc v kostele, aby viděl, jak o půlnoci slouží svatý Václav ve své kapli svatou mši. To se mu však nesplní.

Některé z povídek zase zachycují život na Malé Straně. Tady je to ***Týden v Tichém domě***. Popisuje týden života ve zdánlivě tichém domě, ve kterém se ale po týdnu hodně změní. Zemře stará Žanýrka, Bavorová vyhraje v loterii, pan doktor se ožení s Klárou, dcerou pana Laknuese, u kterého bydlel, i když psal zamilované básně Josefínce. Ale ani ty básně nevymyslel, protože je opsal od mladíka Václava Bavora, jenž mu je dal k posouzení. Ten u něho pracoval jako praktikant, ale raději se věnoval psaní. Popisuje také rodinu domácích a jejich dvě dcery. Domácí se jmenují Ebrovi. První z dcer je vdavek chtivá a má si brát nadporučíka Kořínka, který ji však opustí. Povídka se zakončuje svatbou Josefínky.



#### Týden v tichém domě  
Rozděleno do 25 kapitol. Odehrává se, jak název napovídá, v rámci jednoho týdne. Vyskytuje se v něm velký počet hlavních i vedlejších postav. Ústřední roli zde hraje rodina Bavorových - syn Václav má zobrazovat mladého [Nerudu](http://www.cesky-jazyk.cz/zivotopisy/jan-neruda.html), jeho matka si musí přivydělávat praním u domácích, ale i tak nemá syn na studia. Pracuje proto prozatím v úřadě u pana domácího. K dalším nájemníkům patří slečna Žanýnka (umře na začátku povídky) a slečna Josefínka, o kterou má zájem pan Loukota, avšak jeho láska k ní není opětována. Václav má být později propuštěn z úřadu a vzájemné vztahy jednotlivých postav se postupně zamotávají. Určité rozuzlení přichází na konci, když paní Bavorová vyhraje loterii, čímž její rodina stoupne v očích ostatních lidí z ulice a Václav může dostudovat. Povídka nemá jedno hlavní téma, autor prostě popisuje všední život na Malé Straně.

#### Pan Ryšánek a pan Šlégl  
Kratší povídka, zápletka se odvíjí od dlouholetého nepřátelství pánů Ryšánka a Šlégla. Oba kdysi měli zájem o stejnou ženu, s níž se nakonec oženil pan Šlégl. Bývalí sokové spolu nyní nepromluví ani slovo, ačkoli každý večernavštěvují stejný hostinec. Když jednou Ryšánek onemocní, uvědomí si pan Šlégl, že si na něj po těch letech již vlastně zvykl. Poté, co se Ryšánek uzdraví, mu Šlégl nabídne tabák a začnou spolu opět hovořit. Povídka naráží na malichernost některých lidských rozepří a na neochotu tyto antipatie překonávat. Vypravěč (autor) zde vystupuje pouze jako nejmenovaný host, jedná se proto o ich-formu, ačkoli to není v některých částech povídky zcela zjevné. Kompozičně je rozdělena na dvě části - první část rozebírá zápletku, druhá postupně směřuje k rozuzlení.

#### Přivedla žebráka na mizinu  
Pan Vojtíšek, slušný a proto oblíbený malostranský žebrák, se potká před chrámem sv. Mikuláše s žebračkou, jež se chce přiživit na jeho popularitě. Vojtíšek ji odmítne a ona o něm ze zlosti začne rozšiřovat klepy, že má na Starém Městě nemovitosti a na chudáka si jen hraje. Drby se brzy rozšíří, Vojtíšek přestane dostávat almužny a přesune se do jiné čtvrti. V zimě ho najdou umrzlého na Újezdě, vyhublého a v chatrném oblečení. Autor se v této povídce připomíná, jakou sílu mohou mít pomluvy, obzvláště pokud je šíří pomstychtivá osoba.

#### O měkkém srdci paní Rusky  
Podobně jako předešlá povídka se i tato zaměřuje na pomluvy. Hlavní postavou je zde vdova po hostinském paní Ruska, jejíž jedinou zábavou je chodit na pohřby a pomlouvat nebožtíky. Jednou dojde pozůstalým trpělivost a zavolají na ni policii. Od té doby je paní Ruska terčem posměchu. Nepříjemnou situaci vyřeší přestěhováním k Újezdské bráně, kudy prochází smuteční průvody a kde může dále šířit klepy.

#### Večerní šplechty  
Zde Neruda (v povídce pod pseudonymem Hovorka) vzpomíná na svá vysokoškolská studia a schůzky se spolužáky z ročníku, které pořádali na střeše domu U Dvou slunců. Na rozdíl od ostatních povídek tu najdeme velké zastoupení přímé řeči, hovorový jazyk, studentský slang. Rozhovor mladíků končí, když zjistí, že je celou dobu tajně poslouchala děvčata, za nimiž se ihned vydají.

#### Doktor Kazisvět  
V této povídce hraje majoritní roli maloměšťácký strach z cizinců a jakékoli odlišnosti. Hlavní postava, doktor Heribert, je podivín, jenž odmítá léčit a vůbec stýkat se s lidmi. Ostatní ho nezdraví a nemají ho v úctě. Když však během pohřbu rady Scheplera došlo k nehodě a nebožtík vyklouzl z rakve, zjistil kolemjdoucí Heribert, že je starý pán ještě živý. Pan Kejřík, který měl ze smrti rady prospěch, ho okamžitě nazval Kazisvětem. Tato přezdívka doktorovi zůstala, ačkoli nyní ho již lidé zdravili a vůbec u nich stoupl v ceně. On se však nijak nezměnil a nemocné stále odmítal.

#### Hastrman  
Jediná vskutku pozitivně laděná povídka. Hlavním hrdinou je zde pan Rybář, přezdívaný hastrman kvůli svému zelenému kabátku. Doma si uchovává sbírku kamenů, o které se jeho známí domnívají, že má vysokou cenu. Když se pan Rybář dozví, že se nejedná o drahokamy, zesmutní a rozhodne se sbírky zbavit. Všimne si ho však jeho synovec a ujistí ho, že ho rodina bude mít ráda stále, i když není bohatý. 

#### Jak si pan Vorel nakouřil pěnovku  
Krátká povídka s námětem podobným Doktoru Kazisvětovi, ale s tragickým koncem.  
Krupař pan Vorel se přistěhoval do Ostruhové ulice a zřídil si v ní malý obchod. Místní jsou však zvyklý na zaběhnutý řád a odmítají k němu chodit nakupovat. Po návštěvě slečny Poldýnky se rozšíří zvěst, že je jeho zboží cítit kouřem - od té doby k němu již nikdo nepřijde. Pan Vorel přestane splácet dluhy a nakonec se oběsí.

#### U tří lilií  
Vzhledem k rozsahu dvou stránek se nejedná o povídku, ale spíše o črtu.  
Autor si na taneční zábavě vyhlédne jednu dívku, ale když se s ní chce blíže seznámit, přijde pro ni její sestra a dívka odchází. Zanedlouho se však vrací a překvapenému vypravěči oznámí, že důvodem její nepřítomnosti byla smrt její matky. Romantická a depresivní atmosféra povídky je dotvářena silnou bouřkou, která nabývá v závěru příběhu svého vrcholu.

#### Svatováclavská mše  
Další z povídek psaná v ich-formě.  
Neruda vypráví svůj příběh z dětství, když se nechal přes noc zavřít v katedrále svatého Víta, neboť věřil, že zde v noci slouží mši duchové králů. Ze strachu a hladu však v chrámu usne a probudí se až na ranní mši, kde spatří svoji matku a tetu a jde za nimi.

#### Jak to přišlo, že dne 20. srpna roku 1849, o půl jedné s poledne, Rakousko nebylo rozbořeno  
Spíše delší povídka, v níž autor opět v ich-formě popisuje, jak chtěl jako čtrnáctiletý spolu s kamarády svrhnout Rakousko. Chlapci měli v úmyslu koupit si za našetřené peníze bambitku a střelný prach od hokynáře Pohoráka a začít revoluci z Prahy. Pohorák však nepřijde na smluvené místo, revoluce se tedy nekoná. Později se kluci dovědí, že se hokynář na trhu opil a policie ho odvedla. Když po něm chtějí zpět peníze, kterými mu předem zaplatili, Pohorák vše zapře.

#### Psáno o letošních Dušičkách  
Neradostný příběh o slečně Máry, která každý rok nosila smuteční věnce na hroby dvou mužů. Jednalo se o její bývalé nápadníky, rozhazovačné mládence a dobré přátele, mezi nimiž si Máry nemohla vybrat. Když po nich chtěla, ať se nějak vyjádří, řekli oba, že nemohou zradit kamaráda a přebrat mu dívku. Ani jeden z nich o ni dále nejeví zájem. Dva roky nato jeden z nich, Jan Rechner, zemřel. Slečna Máry se poté domnívala, že se s ní ožení ten druhý, pan Cibulka, který ovšem zemřel o několik měsíců později. Od té doby chodí slečna Máry vždy o Dušičkách na oba předčasně zesnulé mládence vzpomínat.




### Tematicky podobná díla
- Karel Čapek – *Povídky z jedné a z druhé kapsy* (1929)
- Jaroslav Hašek – *Paměti úctyhodné rodiny a jiné příběhy* (1925)
- Ernest Hemingway – *Za našich časů*