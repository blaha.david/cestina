# [Karel Čapek](/Autori/Karel_Capek.md)
## Bílá nemoc (1937)
- drama (divadelní hra)
- tagédie
- **tématika:**
	- **Varování před nebezpečím Fašismu**
	- konflikt mezi demokracií a diktaturou
	- Sebeušlechtilejší člověk nezmůže nic proti zlu a proti rozvášněnému davu
		- *Galén, který přesvědčil maršála, aby ukončil válku, je však ubit fanatickým davem, když odmítá provolávat slávu válce, a on a jeho lék proti bílé nemoci je rozdupán*
- Čapek poté pronásledován gestapem
- Dílo je stále aktuální. Lidé toužící po válce a moci se nezastaví před ničím
- *Bílá nemoc* – nemoc, která postihuje lidi středního věku, je nakažlivá a nevyléčitelná. Lék na tuto nemoc může přinést světu mír.
	
### Obsah
- většina děje se odehrává v nemocnici ve fiktivním státě (podobném Německu a Itálii) někdy před 2. sv. válkou
0. **Autorova předmluva**: autor hovoří o poválečném stavu, kdy jednotlivec nic neznamená. Zdůvodňuje, proč nezvolil konkrétní nemoc a konkrétní zemi – situace zobrazená ve hře se dá aplikovat na více států.
1. **První jednání:** V klinice se tři malomocní lidé baví o tzv. Bílé nemoci, kterou onemocněli, a která se projevuje bílou skvrnou někde na těle a odpadáváním masa, která postihuje osoby starší čtyřicet let, nedá se léčit a na tu již zemřelo 5 miliónů lidí. Dvorní rada Sigelius podává informace o nemoci novinářovi. Pak přichází doktor Galén s tím, že dokáže tu nemoc vyléčit a Sigelius mu nejprve nevěří, ale nakonec mu svěří pokoj č. 13 s nejhoršími případy. Galén je vyléčí, a tak Sigelius požádá doktora, aby mu vyléčil jeho soukromého pacienta, ale Galén odmítne ho léčit, protože je bohatý. Potom do nemocnice dorazil maršál a ministr zdravotnictví, kteří jsou nadšeni úspěchem kliniky. Na ten zázrak se přijedou podívat také novináři, kterým Galén sdělí svůj vzkaz pro všechny lidi a to, že je ochotný vyzradit svůj lék, pokud se všechny státy zavážou, že nebudou válčit, a pokud ne, bude dál léčit jen chudé. A potom za to Sigelius doktora Galéna vyhazuje z kliniky.

2. **Druhé jednání:** Baron Krüg, který taky onemocněl tou nemocí, přichází za Sigeliem a ptá se ho, jak pokročilo léčení nemoci. A tak se převleče za žebráka a jde ke Galénovi, který ho okamžitě pozná a chce po něm totéž, co po ostatních – světový mír. Potom Krüg hovoří s maršálem o své nemoci. Maršál je krutý, a tak přikazuje zvýšit výrobu zbraní a munice, a o míru nechce slyšet. Galén je pak předvolán k maršálovi a maršál hrozí zatčením a mučením, pokud Galén nebude léčit Krüga, ale Galén se mu nepodčiní. No a potom po telefonu maršálovi někdo oznamuje, že baron Krüg se zastřelil.
  
3. **Třetí jednání:** Jde diskuze mezi ministrem zdravotnictví a maršálem o svých možnostech a shodnou se na rychlém útoku na nepřátele po kterém by mohl přijít mír. Potom maršál, který je již oslaben bílou nemocí promlouvá k národu. Jeho dcera Aneta a její milý Pavel, syn barona Krüga, mu s maršálovým plánem pomáhají. Potom Pavel převezme zprávy o postupujícím boji, které nejsou příznivé. Aneta a Pavel přesvědčí maršála, aby Galénovi slíbil mír, ale když doktor Galén vystoupí z auta, aby donesl lék maršálovi, obestoupí ho rozvášněný dav chtíči (bažící) po válce, který vede Pavel. Galén je ušlapán i se svými léky.


### Postavy
**Doktor Galén** *(dětina)* - šlechetný, neústupný, je proti válkám. Léčí pouze chudé - zadarmo. Bohaté bude léčit pouze za mír.

**Maršál** – velitel vojsk, bezohledný, nelidský (diktátor)

**Baron Krüg** – ředitel továrny na výrobu zbraní, vydělává na utrpení druhých, onemocní bílou nemocí a potom se zastřelí.

**Prof. Dr. Sigelius** – ředitel kliniky, vypočítavý, nejradši by si Galénův objev přivlastnil a vydělal na těm
  
***Anonymní rodina*** *(Otec, matka, syn)* – bezejmenná rodina, pracují u barona v továrně, onemocní bílou nemocí.