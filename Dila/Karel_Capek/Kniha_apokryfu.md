## Kniha Apokryfů

- z řečtiny: apokryfos = zatajovaná kniha
- vychází se z bible
	- podobné postavy
	- jiný čas a místo děje

### Marta a Maria
- autor vede polemiku s biblickým textem
- jazykové prostředky:
	- epiteton - básnický přívlastek (*živý plamen*)
	- archaismy: vidouc, řkouc, kteráž
	- slovní inverze
	- apoziopeze - nedokončená výpověď
		- pomlčky nebo tři tečky
	- šestinedělí = po porodu
	- Elipsa: ve větě je vypuštěná část, která je ale zřejmá z kontextu (*"Byl."*)
	- hovorovost - (*Aby mně **jako** pomohla*)


