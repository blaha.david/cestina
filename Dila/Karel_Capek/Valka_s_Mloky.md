# [Karel Čapek](/Autori/Karel_Capek.md)
## Válka s Mloky (1935)
- antiutopický román (delší než Bílá nemoc - drama)
- **mloci - alegorie nacistů a fašismu**
- tématika:
	- osidlování pevniny mloky
	- mloci tu symbolicky zastupují rozpínající se fašismus, který se ve 30. letech rozmáhá, upozornění i na zneužití vědy a techniky jimi.
	- narážky na nacismus a na vznik 2. světové války
- er-forma (dialogy - přímá řeč - ich-forma)
- odborné texty - pryvky novinových článků
- anglické výrazy
- hodně popisu, o děj jako takový moc nejde - důležité jsou karikatury
- je rozdělena do 3 kratších "knih":
	1. *Andrias Scheuchzeri*
		- vypravování, jak vše začalo
		- nejvíc dějová
	2. *Po stupních civilizace*
		- vypravování, rozšiřování mloků po světě
		- novinové články - koláž - ankety, studie, eseje, úvahy, ...
		- paroduje různé skupiny lidí
	3. *Válka s mloky*
		- závěrečný konflikt
		- negativní utopie
		- zhodnocení
		- reportáž

### Postavy
- ***kapitán van Toch***
	- vlastní obchodní loď a hledá perly
	- objevitel mloků
- ***Bondy***
	- průmyslník
	- financoval projekt pana van Tocha
- ***pan Povondra***
	- zaměstnanec Bondyho - vrátný
	- „zavinil“ projekt pana van Tocha (vyčítá si to)
	- v 2. knize sbírá novinové články a představuje pohled občana
- ***mloci***
	- nacisti
- ***Chief Salamander***
	- velitel mloků
	- *Hitler*



### Děj
- odehrává se po celém světě před 2. sv. válkou
- (ostrovy v Tichomoří, Čechy, zoo v Anglii apod.) 


Příběh se odehrává před 2.sv.válkou. Začíná příjezdem kapitána Van Tocha na ostrov Tana Masa, který leží v zátoce Devil Bay, aby tam lovil perly. Je to jediné místo, kde ještě perly nacházejí. Dozvídá se o tom místě od místních, kteří taky říkají, že je to zátoka žraloků. Kapitán tomu nevěří a vezme sebou na výpravu i dva potápěče, kteří když se vynoří, jsou v šoku a tvrdí, že tam jsou mořští čerti.

Kapitán se na lodi večer opije a jde se projít na pobřeží, kde z moře začínají vycházet mořští čerti. Kapitán si je oblíbí a dá jim nůž na ochranu proti žralokům a mloci mu za to začínají pomáhat s lovením perel.

Stráví tam s nimi rok. Poté se vrací do Čech, kde jde navšívit průmyslníka pana Bondyho, který mu pomůže založit Pacifickou Exportní společnost, jež rozšiřuje mloky po celém ostrově a prodávají perly, které mloci vylovili.

O mlocích se zatím dozvídají vědci a mloci podstupují mnoho experimentů, z kterých vyplývá, že to jsou inteligentní tvorové, rychle se rozmnožující.

Po několika letech výlovu kapitál umírá a pan Bondy začne mloky prodávat jako levnou pracovní sílu. Používají se k budování nových pevnin, podvodních pevností. Lidé jim dávají zbraně a krmí je. Stále se rozmnožují a je jich stále víc a víc, stavějí se pro ně školy.

Po nějaké době se začíná objevovat první napadení lidí mloky a roste napětí. Chief Salamandr kontaktuje lidi a požaduje zbourání postavených pevnin ve prospěch mloků. Lidé se bojí o své životy a prchají do hor. Jeden mlok byl spatřen i ve Vltavě.

V závěru románu autor rozmlouvá se svým vnitřním hlasem, jenž ho nabádá, aby nenechal lidskou civilizaci zaniknout. Autor tak přichází s několika alternativními konci. Nakonec dojde k tomu, že se mloci zapletou do národnostních sporů a začnou mezi sebou bojovat, až se vyhubí a svět bude zase jako dřív.

