# [Karel Havlíček Borovský](/Autori/Karel_Havlicek_Borovsky.md)

## Král Lávra
- **Výrazová forma**: poezie
- **Literární druh**: epika
- **Literární žánr**: satirická báseň, 1. a 3. verš se nerýmuje (nepravidelné schéma ABCBDDB)
- **Slohový postup**: vyprávěcí
- **Typ vypravěče**: er-forma
- **Hlavní téma**:
	- zesměšnění vrchnosti
	- každý člověk dělá chyby a nemusí se stydět za to, jak vypadá
- **jazykové prostředky**: hovorové a knižní výrazy, archaismy, lidová mluva, přímá řeč, přirovnání
- vypravěč je **lyrický subjekt**
- pochází ze staré Irské pověsti (paroduje ji)
- nejmírnější
- film - 1950 - Karel Zeman - loutkové



### Charakteristika postav

- #### Král Lávra
	-  dobrý panovník
	-  nechce, aby nikdo věděl jeho tajemství - má oslí uši

- #### Kukulín
	- holič
	- čestný, spravedlivý
	- smířený s osudem
    
- #### Kukulínova matka
	- vdova
	- milující matka
	- zachránila syna před šibenicí




### Obsah
- král má oslí uši
- holí se jednou ročně
- holiče vždy popraví, aby to neprozradil
- Kukulínova matka ho přesvědčí, aby ho nechal žít a dělal mu holiče trvale
- Kukulín to řekne vrbě
- basista (Červíček) ztratí kolíček a vyřeže si ho z vrby
- basa to prozradí