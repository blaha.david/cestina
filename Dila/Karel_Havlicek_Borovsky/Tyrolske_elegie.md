# [Karel Havlíček Borovský](/Autori/Karel_Havlicek_Borovsky.md)

## Tyrolské elegie
- poezie, lyricko-epická báseň, nejdrsnější
- politická satira
- ich-forma - vypravěčem je sám Borovský
- elegie = **žalozpěv**
- oslovuje měsíc = **apostrofa**
- **ironie** - *"Bach mi píše jako doktor"*
- retrospektiva - vzpomínky
	- pravdivý příběh
	- deportace do Brixenu
- 19\. st. - Bachův absolutismus
- zesměšňování vlády
- stesk po domově a rodině
- lidová "básnička" - lidová mluva, obecná a hovorová čeština, zdrobněliny
- paroduje pohádku


### Obsah
- popisuje jeho cestu do rakouského Brixenu
- devět zpěvů:
	1. promlouvá k měsíci
	2. návštěva policajtů
	3. čte dopis a dozvídá se, že kvůli jeho zdraví musí odjet do Brixenu
	4. policajti mají na spěch, nemusí si s sebou brát žádné zbraně - oni ho ochrání
	5. loučení s rodinou
	6. vyráží na cestu za dohledu četníků
	7. vypráví o cestě, městěch a hradech, které potkali
	8. koně se splašili, policajti zpanikařili a vyskákali z kočáru - do Brixenu přijíždí sám, policajti za ním pěšky
	9. vězení v Brixenu