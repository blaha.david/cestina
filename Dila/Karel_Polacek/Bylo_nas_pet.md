# [Karel Poláček](Autori/Karel_Polacek.md)

## Bylo nás pět
- povídka? novela? román?
- **motiv**:
	- obraz maloměsta
	- zobrazení světa dospělých dětskýma očima, které nepřehlédnou žádnou faleš a pokrytectví
	- spisovatel se snaží lidem ukázat dětskou nevinnost a představivost
	- každý, kdo si přečte knihu, určitě zavzpomíná na staré časy z mládí
- tuto knihu napsal v nejtěžších chvílích svého života
	- bál se transportu do koncentračního tábora
	- aby zahnal smutek, napsal knihu o svém dětství


### Charakteristika postav
- **Péťa Bajza**
	- vypravěč
	- malý, veselý, živý (někdy až příliš) kluk
	- má spoustu nápadů a bujnou představivost
	- má staršího bratra a mladší sestru Mančinku
- **Maminka**
	- hodná, laskavá, rozumná a klidná
- **Tatínek Bajza**
	- vlastní obchod
	- laskavý, ale přísný - snaží se být synovi vzorem
- **Kristýna** *(Rampepurda)*
	- Bajzovic služka, hezká a milá dívka
- **Zilvar**
	- kluk z chudobince, který se nezatěžoval tím, že mluví jako dlaždič
- **Éda Kemlink, Tonda Bejval, Čeněk Jirsák**
	- kluci z party, kteří se občas pohádají, ale pořád drží spolu
- **Otakárek**
	- bohatý chlapec, při těle, osamocený, hodně čte
- **Eva Svobodová**
	- dcera cukráře Svobody
	- Péťova favoritka
- **pan Fajst**
	- starší pán
	- chce po mládeži úctu a vychování, a dělá jim proto problémy
- **Pajda**
	- pes Édy Kemlinka

### Obsah
- první republika
- Rychnov nad Kněžnou

Petrovi rodiče vlastní obchod. Petr má malou sestřičku Mančinku a staršího bratra, který už s nimi nebydlí, ale pracuje na německých hranicích. Petr se pořád z legrace pere se služkou Kristýnou, které říká Rampepurda. Dělají si naschvály - například Petr jí dá do kufru mrtvou myš, aby se ulekla. Ale potom dostane vynadáno a brečí. Petr a jeho čtyři kamarádi prožívají různá klukovská dobrodružství. Ve městě je biograf, kam kluci rádi chodí. Lezou tam  tajně, aby nemuseli platit. Kluci si také chtějí ochočit vosy, ale to se jim nepodaří. Kluci čtou dobrodružné knížky a chtěli by cestovat - plánují si cestu do cizí země, ale samozřejmě ji neuskuteční. Chodí se koupat k rybníku, kde Zilvar zachrání tonoucího chlapce Vénu. Kluci se perou s nepřátelskými Ješiňáky a Habrováky. Dělají si legraci z pana Fajsta, který věčně pomlouvá mládež, která ho nezdraví. Petr byl také několikrát navštívit Otakárka, který chce kamarády, ale nemá je. Petr slíbil mamince, že bude paní Soumarové říkat "rukulíbám", aby ukázal svoje dobré vychování, ale styděl se a neřekl to. Soumarovi jsou bohatí a mají krásný dům. Petr si od Otakárka půjčuje knížky. Ale Otakárkova vychovatelka Petra kritizovala a bála se, že od něj Otakárek chytí špatné mravy.  
Příbuzní Vařekovi jdou občas Bajzovi navštívit. Bajzovi před nimi skrývají, že mají k obědu husu, protože by je za to Vařekovi kritizovali, že mají maso. Paní Bajzová tvrdí Emě Vařekové, že mají brambory, ale nakonec Ema zjistí, že mají husu, a Vařekovi se velice urazí. Celá rodina se tomu směje.  
Když do města přijede cirkus, Petřík touží vidět cizokrajná zvířata, proto je velmi hodný, pozorný, poslušný, nápomocný a vzorný, až to rodičům připadá podezřelé a myslí si, že onemocněl. Učí se, hraje na housle, nechodí ven s kluky, hlídá Mančinku a nepere se s Rampepurdou. Když byl ve městě cirkus, jeden chlapec z cirkusu, Alfons Kasalický, s nimi začal chodit do školy. Kluci jsou hrdí, že s ním mohou kamarádit. Petřík nakonec přesvědčí rodiče, aby všichni šli jednou do cirkusu. V cirkuse byl i Zilvar z chudobince, který u cirkusu pracoval, a proto měl dovoleno ho navštívit. Petr a statní kluci chtěli v cirkuse taky vypomáhat, proto nosili zvířatům vodu. Přišel za nimi i Otakárek a taky jim pomáhal. Ale přišla jeho vychovatelka a udělala hysterickou scénu, že Otakárek je z bohaté rodiny a nebude dělat takovouhle práci. Ředitel cirkusu ho proto vyhodil a ostatní hochy také.  
Petr onemocněl spálou. Zdály se mu dobrodružné sny, kterým věřil. Ve snu mu tatínek koupil slona indického Jumba, který uměl mluvit. Slon byl obdivován celým městem a přál si červenou čepici protkanou zlatem s bambulkami. Tatínek Petrovi slíbil, že mu čepici zaplatí. S kluky se na Jumbovi vypravili do Indie. Navštívili krejčího, který pocházel z Čech, a seznámili se s maharádžou a princeznou. Zilvar se oženil s princeznou a kluci se na svatbě poprali. Když se ale trochu vyléčil, došlo mu, že se mu celé to dobrodružství asi jenom zdálo.