# [Ladislav Stroupežnický](/Autori/Ladislav_Stroupeznicky.md)

## Naši furianti
- první české realistické drama
- obraz života v české vesnici Honice v roce 1887
- **motiv**:
	- mezilidské vztahy
	- výsměch vesnických boháčů - furiantů
- *Furianst* = egoista, náfuka
- dialogy, hovorový jazyk, jihočeské nářečí

### Postavy:
- **Valentin Bláha** – voják, poctivý
- **Josef Habršperk** – přítel Valentina, poctivý
- **Filip Dubský** – starosta, spravedlivý
	- syn **Václav** – miluje Verunku
- **Jakub Bušek** – první radní, pytlák, egoistický
	- dcera **Verunka** – miluje Václava
- **František Fiala** – podvodník, vychytralý, nepoctivý
	- 7 dětí (Krystýna - hloupá, nechápavá, neumí zpívat; ...)
### Obsah
- Fiala a Bláha se ucházejí o ponocenství
- Bláha a Habršperk jsou "vzdělání" lidi - poznali svět 
- Všechny ostatní považují za nevzdělané lidi. 
- Bušek je pro Fialu
- Dubský pro Bláhu
- Kristýna napíše paličskou ceduli jako "od Valentina" 
- Pohádají se a zakážou dětem svatbu. Václav se naštve a odchází na Vojnu. 
- Habršperk si nechá od Kristýny opsat písničku, podle toho pozná stejný rukopis jako na paličské ceduli
- Bláha dostává ponocenství, Fiala přísahá že už bude poctivý a ceduli roztrhají
- Habršperk uvidí Buška pytlačit a dohodnou se, že ho neudá a on nechá Verunku s Václavem