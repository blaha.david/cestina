# [Moliére](/Autori/Moliere.md)
## Lakomec
- [drama](/Dramaticky_text.md) (psáno jako divadelní scénář)
	- klasicistní komedie o 5 dějstvích
- Moliere chce dokázat, jak peníze deformují přirozené lidské vztahy
- dialogy - ich-formy
- nejproslulejší Moliérova hra
- inspirovaná *Komedií o hrnci* - Plautus
- jednota děje, času a prostoru (žádné skoky)


### Charakteristika postav
- #### *Harpagon*
	- ústřední postava = lakomec
	- šedesátiletý lichvář, vdovec *(latinsky harpago = kořistník)*
	- chamtivý, lakomý, bezcitný, podezřívavý
	- pro peníze obětuje úplně vše (vztahy, děti, rodinu, ...)
		- omezuje své děti, okrádá služebnictvo
		- intrikuje se vztahy svých dětí a jejich protějšky
	- ztráta peněz pro něj znamená šílenství, konec smyslu života
	- ***Kleant***
		- jeho syn
		- milenec Mariany
		- opakem svého otce - touží po lásce, peníze jsou pro něj nedůležité
	- ***Elisa***
		- jeho dcera
		- milenka Valera
		- opakem svého otce - touží po lásce, peníze jsou pro ni nedůležité
- #### *Anselm*
	- Tomas d'Alburci z Neapole
	- ztracený otec Mariany a Valera
	- ***Mariana***
		- jeho ztracená dcera
		- milenka Kleanta, milována Harpagonem
	- ***Valer***
		- jeho ztracený syn
		- milenec Elisy, zachránil ji před utopením
		- vetřel se do domu Harpagona jako sluha správce, aby mohl být s Elisou
- ***La Fléche*** (čipera) - sluha Kleanta, vynalézavý, ukradl peníze
- ***Frosina*** - *pletichářka* = zprostředkovatelka (obchodů,), dohazovačka
- ***Mistr Jakub*** – Harpagonův kuchař a kočí

### Obsah
- Děj se odehrává v Paříži kolem roku 1670
1. První dějství – děti Harpagona, Eliška a Kleant, se domlouvají, jak říct otci, že mají partnery; Eliška sluhu Valéra a Kleant dívku Marianu. Otec jim však sděluje, že se chce oženit s dívkou Marianou a Elišku chce provdat za padesátníka Anselma. 

2. Druhé dějství – Kleant si chce vypůjčit od lichváře, půjčku mu zajišťuje sluha Čipera. Ukáže se, že oním lichvářem je Harpagon.

3. Třetí dějství – Mariana s dohazovačkou přichází velmi nerada do Harpagonova domu na námluvy. Rozzáří se, když zjistí, že její milý Kleant je Harpagonův syn.

4. Čtvrté dějství – oba páry se domlouvají, jak odradit Harpagona od sňatku. Čipera schoval Harpagonovi pokladnici, a tím obrátil jeho pozornost na peníze. 

5. Páté dějství – zatímco komisař vyslýchá všechny v domě, na večeři přichází hrabě Anselm. Odhaluje se, že je otcem Valéra a Mariany a je natolik bohatý, aby zaplatil obě svatby.
