# [Paulo Coelho](/Autori/Paulo_Coelho.md)

## Alchymista
- Originál 1988 *(O Alquimista)*
- Psychologický román
- **Magický realismus**
	- Literární směr
	- Latinsko americká literatura
	- Prolínání nadpřirozeného světa s reálným světem
	- Lidová slovesnost
	- Mýtické prvky - vnímány jako přirozená věc
- Děj se odehrává ve Španělsku, Africe a Egyptě
	- *Andalusie - historické území na jihu Španělska*
- hlavním tématem je vnitřní (psychologický) stav ústředních postav
	- Ty jsou obvykle velmi složité, ale čtenář se z příběhu postupně dozvídá příčiny jejich myšlenek i chování a na konci je obvykle odhalení nebo osvobození od nějaké duševní trýzně
- obecná čeština - hovorovost (přímé řeči)


### Charakteristika postav
- #### *Santiago*
	- hlavní postava
	- Mladý pastýř
	- Odvážný
	- Vzdělaný - umí číst
	- Vnímavý
- #### *Cikánka*
	- Prospěchářská
	- Pravdomluvná
	- Proroctví
- #### *Sálemský král*
	- Moudrý
	- Ví všechno
	- Zná věci, které ostatní nechápou (Řeč světa)
- #### *Angličan*
	- Nadšený, horlivý
	- Chce se stát alchymistou
- #### *Sklenář*
	- Snílek
- #### *Fátima*
	- Žena pouště
	- Slušná, zdvořilá, věrná
- #### *Alchymista*
	- Moudrý
	- Učí Santiaga Řeči světa


### Děj
-   Sen
-   Cikánka
-   Sálemský Král
-   Afrika
-   Sklenář
-   Oáza
-   Alchymista


### Myšlenka díla
Každý člověk má svůj Osobní příběh. Srdce každému napoví, co je jeho Osudem a za čím má člověk jít. Santiago měl v Osobním příběhu, že má najít poklad. Sice ho měl doma, ale Osud ho zavedl až do Egypta, protože se musel naučit a poznat některé věci - musel se naučit mluvit se svým srdcem, pochopit zákony života, vidět pyramidy, potkat Fátimu a Alchymistu. Santiago měl možnost vzdát se svého snu, ale neudělal to. Kdyby to udělal, nesplnil by si svůj Osobní příběh.

Někdy se dějí věci, které se nám zdají špatné a zbytečné. Nechápeme, proč se dějí. Někdy je lepší neptat se, proč se staly. Později to pochopíme. Některé "špatné" věci nám zachraňují život.

Člověk, který příliš sní, vykresluje si svůj sen různými barvami, fantazíruje a představuje si sebe i druhé při splnění svého snu, si ho nechce uskutečnit. Kdyby si ho vyplnil, neměl by proč žít. Takový člověk si nesplní svůj Osobní příběh.

V životě potkáváme lidi, kteří hrají nějakou roli. I když nechápeme, proč se dostali do našeho života, jsou důležití. Nemusíme chápat proč.

Kdo chce žít Osobní příběh druhého a kdo závidí, neprožije si ten svůj.