# [Viktor Dyk](/Autori/Viktor_Dyk.md)

## Krysař (1915)
- epická **novela**
- kritika měšťáctví
- realismus, prvky romantismu, symbolismu a dekadence
	- tímto způsobem také psali například: Fráňa Šrámek, Gellner, Petr Bezruč
- Dyk používá symboly – což jsou prvky neoromantismu
- **Hlavní myšlenka**:  střet jedince a společnosti

### Symboly:
-   píšťala - symbol moci
-   krysy - přetvářka, společenská špína
-   sedmihradská země - nebe
-   Sepp Jörgen - symbol čisté duše - dá přednost dítěti

### Charakteristika postav
- #### *Krysař* (nemá jméno)
	- hlavní postava
	- tajemný, osamělý poutník
	- má jen svou píšťalu - živí se vyháněním krys
	- stojí sám proti ostatním
	- usiluje o Ágnes - smysl života
- #### *Ágnes*
	- pohledná, krásná, citlivá dívka
	- opětuje lásku krysaři
	- milenka Kristiána
	- zoufalá, nakonec spáchá sebevraždu
- #### *Sepp Jörgen*
	- mladý, pohledný, urostlý muž
	- rybář
	- osamocený, zaostalý - zpomalený
	- hodný
- #### *Dlouhý Kristián*
	- majetnický
	- milenec Ágnes
	- chce si získat Ágnes pro sebe skrze peníze
	- neví o krysaři a Ágnes



### Obsah
německé město Hammeln; středověk - historický čas
- Do města Hammeln přichází krysař s píšťalou, aby z něho vyhnal krysy.
- Zamiluje se do mladé Ágnes, která se stává smyslem a láskou jeho života.
- Nechtějí mu však zaplatit za jeho práci ani domluvenou částku - sto rýnských.
- Rozzuřený odmítne menší částku a řekne jim, že se pomstí - odchází.
- Když se po chvíli vrací zpět do města, tak ho něco začne táhnout do tamější hospody.
- Najde tam záhadného cizince celého v černém.
- Jmenuje se Faustus z Wittenberka a pracuje pro ďábla.
- Říká mu, že jeho flétnou může dělat daleko lepší věci, než je vyhánění krys.
- Ágnes je však těhotná s Kristiánem, jehož nemiluje, neunese to a utopí se v řece.
- V krysařovi se vše zlomí, a tak zapíská na píšťalu ze všech sil.
- Všichni lidé z města šli za ním do propasti, až na rybáře Jörgena, jenž zachraňuje malé dítě.