# [William Shakespeare](/Autori/William_Shakespeare.md)

## Romeo a Julie
- [drama](/Dramaticky_text.md)
- tragédie
- divadelní hra - scénář
- poezie (verše) i próza
- starý básnický jazyk
- slovní inverze
- téma: Jak rodová nenávist dokáže zmařit život mladých lidí.
- motiv: Tragická (ne nešťastná) láska; zbytečnost krvavých sporů; spor Martelů a Kapuletů; souboje a smrt
- člení se na prolog(v podobě sonetu) a 5 dějství
- Nezúčastněný vypravěč - er-forma, Repliky - přímá řeč postav - ich-forma.
- dialogy


### Charakteristika postav
- Romeo i Julie v příběhu mentálně dozrávají

- #### *Julie*
	- dcera Kapuletova
	- 14 let
	- věrná, statečná, citlivá, pěkná
	- bojuje o právo na lásku

- ### *Romeo*
	- syn Montekův
	- hluboce zamilován do Julie
	- typický platonický milenec
	- odhodlaný pro ni obětovat vše
	- čestný, citlivý, romantický
	- jedná ukvapeně, bezmyšlenkovitě a zaslepeně

- #### *Tybalt*
	- Juliin bratranec
	- bojovný, útočný, provokativní
	- ctí své jméno

- #### *Chůva*
	- spolehlivá, důvěryhodná
	- nerozumná
	- pomáhala Julii

- #### *Merkucio*
	- Romeův přítel
	- bojovný, agresivní

- #### *Benvolio*
	- Romeův přítel
	- čestný, upřímný, spravedlivý

- #### *Paris*
	- nápadník Julie

- #### *Otec Vavřinec*
	- Typický renesanční člověk - soucítí s oběma milenci
	- je proti zaběhnutému řádu
	- smysl pro dobro
	- moudrý, ochotný, vzdělaný, čestný



### Děj
- odehrává se ve Veroně (renesančním městě v Itálii) v 16. století
- nešťastný osud dvou milenců, kteří zahynou kvůli sporům mezi jejich rodinami
- spor rodů na náměstí
- maškarní ples u Kapuletů, zasnoubení Julie s Parisem
- Romeo s Merkuciem a Benvoliem se rozhodnou ho tajně navštívit
- Tybalt Romea pozná a chce vyprovokovat bitku, ale starý Kapulet ho zastaví
- Romeo spatří Julii a zamiluje se do ní a ona do něj
- ples skončí a oni na sebe nedokážou přestat myslet
- Romeo tajně navštíví zahradu Kapuletů (balkonová scéna)
- Otec Vavřinec, který je podporuje, je tajně oddá
- Tybalt zabije Merkucia
- Romeo zabije Tybalta a je vyhoštěn z Verony
- Julie se má vdát za Parise, ale odmítne a požádá otce Vavřince o pomoc
- dá jí nápoj, po kterém bude 42 hodin vypadat jako mrtvá a Romeo si pro ní přijde do hrobky a budou žít navěky spolu, ale Romeo nedostal zprávu od posla, že není mrtvá - posel byl cestou zavražděn
- Romeo se vrací do Verony, kde zabije Parise, naposledy políbí Julii a vypije jed
- Julie se probudí, zjistí, že Romeo je mrtvý, vezme jeho dýku a pobodne se
- spor mezi rody skončí, rodiny se usmíří a postaví zlaté sochy Romea a Julie



### Hlavní myšlenka:
- Autor chtěl touto knihou říci, že když se nepřátelé nechtějí usmířit, odnesou to skoro vždy ti nevinní.
- Kniha nabádá k zamyšlení se. Ukazuje nám obraz tehdejší doby a problémy lidí. Nešťastný osud dvou milenců bude vždy aktuální...
- Autor chce rozebrat téma lásky, která není dovolena. V této době si děti vladařů nemohli vybrat partnera, kterého chtěli a milovali, ale partnera, jehož jim určili rodiče, a to na základě smluv a majetku.