# [Zdeněk Jirotka](/Autori/Zdenek_Jirotka.md)

## Saturnin (1942)
- jeho první dílo
- humoristický, satirický román
	- *ne román v pravém slova smyslu – nemá jednotící příběh, pouze sled epizod*
- inspirace anglickými humoristy (Jerome Klapka, Wodehouse)
- odehrává se v praze a na venkově na úpatí Hradové v 30. letech 20. století (atmosféra první republiky - *tenisové turnaje, život na hausbótu, kouření je ještě zdravé*)
- ich-forma (dnes vypravování spíš v er-formě), minulý čas
- vypravěč je hlavní hrdina (beze jména)
- jazykové prostředky: spisovný jazyk, archaická slova, vulgarismy, ironické metafory, přísloví (teta Kateřina)
- charakteristika převážně nepřímá vnitřní
- nepřímá řeč (málo přímé řeči)
- televizní adaptace


### Charakteristika postav:
- postavy se v průběhu díla nevyvíjejí
- #### *vypravěč* (neznáme jméno)
	- mladý pán z Prahy (cca 30 let)
	- poněkud nudný život
	- konzervativní, ale se skrytým smyslem pro nevšední zážitky
	- zamiluje se do slečny Barbory
	
- #### *Saturnin*
	- svět bez hranic, konvencí a běžných omezení
	- tajuplný - neznámý původ
	- sám si určuje, komu bude sloužit
	- hození koblihy jako gesto rozčeření stojatých vod
	- jeho cílem je veselá provokace, nekonvenčnost a barvitost života
	- sám se svým kouskům nikdy nesměje, zachovává vážnou tvář
	- celý život touží dostávat rozkazy neobvyklého rázu
	- vybrané kousky:
		- házení koblih
		- přestěhování se na loď bez vědomí vypravěče
		- vypráví přátelům vypravěče o jeho prožitých dobrodružstvích na lovu
		- lov lva v pražských ulicích
		- vyhnání tety Kateřiny z lodi: škrabošky proti krysám
		- škodolibosti na Milouše: vyslán chytat červy, uvěznění v pokoji, sklapování lehátek, hoření „vousů“, vypnutí jističe

- #### *doktor Vlach*
	- vzdělaný starší muž
	- rozdělení lidí do dvou (tří) skupin podle koblih v kavárně

- #### *dědeček*
	- starý muž
	- zkušený
	- bohatý - baví se tím, jak všichni bojují o jeho přízeň a jeho majetek
	- všechno na elektřinu (bývalý ředitel elektráren?)
	- vyprávní příběhy z mládí

- #### *slečna Barbora*
	- krásná
	- tennis
	- vždy veselá

- #### *teta Kateřina*
	- vdova
	- chamtivá (snaží se přimět dědečka, aby svůj majetek odkázal na ni)
	- přísloví, rčení, lidová moudra - podobnost s Babičkou
	- syn Milouš

- #### *Milouš*
	- je omezený
	- speciální škola
	- také má zájem o slečnu Barboru



### Děj
- vypravěč si najme sluhu a ten mu obrátí život naruby
- loď
- lov lva
- dovolená u dědečka
- slečna Barbora
- teta Kateřina
- kotník
- bouřka
- výlet


### Tematicky podobné dílo:
- Jiří Brdečka: ***Limonádový Joe***
- Jaroslav Hašek: *Osudy dobrého vojáka Švejka*
- Ladislav Pecháček: *Amatéři aneb Jak svět přichází o básníky*

---------------
[Zdenek Jirotka - Saturnin](https://docs.google.com/document/d/1FFYK9CRo48Ne-cQqhRar1yjw-6fnOfjg/edit)
[Saturnin](https://docs.google.com/document/d/1AjSKgr_V0ixgQ9RJ_kd1y8TEjYY4guUR/edit)
[Jirotka Saturnin](https://docs.google.com/document/d/10Ez8pgNmwtVWQ7tRwfPhHXWTGt8Qn063/edit)