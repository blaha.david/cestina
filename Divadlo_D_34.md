
# Divadlo D34

= **režisérské divadlo**

- **velká práce se světlem**
- levicově orientované divadlo
- založeno 1933 Emilem Františkem Burianem
- první divadelní družstvo - divadlo patří všem co tam pracovali
	- **uplatňovali principy komunismu**
- každý rok měnilo název: D + letopočet konce sezóny (skončilo D41)
- hráli i jednodušší hry pro méně zdatné diváky i náročnější hry
	- **klasické hry** - Moliér, Shakespeare
	- **zdramatizovali slavná díla** (*Dobrý voják Švejk*, *Utrpení mladého Werthera*)
- za okupace se věnovali českým textům
- premiéra *Manon Lescaut*
- zavřeno v r. 1941
- obnoveno v r. 1946, od r. 1955 opět jako divadlo D34


## Emil František Burian
- první polovina 20. století (1904 - 1959)
- básník, dramaturg, herec
- nebyl moc dobrým členem KSČ
	- v r. 1941 byl zatčen
	- vězněn na pankráci
- dostal se do koncentračního tábora, jako jeden z mála se z něho vrátil
