# Divadlo Járy Cimrmana
- Žižkov - Štítného 5
- Komické hry (15 her)
- Autorské divadlo
- Neherci
- Svěrák + Smoljak


## Historie
- 1966 - První zmínka o Cimrmanovi
- 1972 - stěhování z Malostranské besedy do sálu Reduty
- 1992 - 1. ročník běhy J. C. - jediný závod se zavazadly v ruce
- 1995 - přejmenování na "Žižkovské divadlo J. C."
- 1998 - planeta č. 7796 pojmenována "Járacimrman"
- 2003 - Svěrák + Smoljak - objev zpravodaje
- 2004 - 10 000 představení
- 2005 - J. C. vyhrál soutěž NEJVĚTŠÍ ČECH
- 2008 - narození Jaroslava Cimrmana (Kladno)


## Cimrmanologové
- **Jiří Šebánek**
	- zakladatel postavy J. C.
- **Zdeněk Svěrák** (1936)
	- UK pedagogika - ČJL
	- naivní vstup do KSČ
	- filmový režisér:
		- *Kolja*
		- *Rozpuštěný a vypuštěný*
- **Ladislav Smoljak** (1931 - 2010)
	- UK pedagogika (MAT+FYZ)
	- *Mladá fronta* – redaktor
	- Námět a scénář
- **Miloň Čepelka** (1936)
	- UK pedagogika - ČJL
	- rozhlasy: ČsRo, ČRo2
- **Karel Velebný**



## Jára Cimrman
- narozen 1856 / 1864 / 1868 / 1883 / 1884 ve Vídni
- "zapomenutý génius"
- renesanční člověk
- vynálezce:
	- projekt Panamského průplavu
	- vynález jogurtu
	- objev CD - Cimrmanův Disk
	- vynález plnotučného mléka
	- J. V. Sládek – oprava závěru básně Lesní studánka
	- označení sněžného muže „Játy“ --> „Yetti“
	- ukrytá rozhledna J. C.
	- maják J. C.
- audiokniha – *Vyšetřování ztráty třídní knihy*


