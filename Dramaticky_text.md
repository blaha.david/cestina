 # Dramatický text
- struktura - rozdělení textu k jednotlivým postavám
 - texty u postav = **repliky** (promluvy)
- scénické poznámky - v závorce, kurzívou
	- aby postavy vědeli, jak mají interagovat s ostatními postavami
- jména postav, které mluví

**JMÉNO POSTAVY**: Text, který postava říká (*Scénická poznámka, co dělá*) 

přerušení řeči postavy = **nedokončená výpověď** = **elipsa**
v textu označeno pomlčkou (někdy také tři tečky)
