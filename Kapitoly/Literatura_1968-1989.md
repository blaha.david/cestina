# Literatura v letech 1968 - 1989
- od r. 1969 --> **normalizační proces**
- **Charta 77**
	- celonárodní dosah
- zákaz publikování ("index")
	- Kundera, Havel, Tigrid, Vaculík, Lustig, [Škvorecký](/Autori/Josef_Skvorecky.md), Mňačko, [Seifert](/Autori/Jaroslav_Seifert.md), ...
- "trezorové filmy"
- řada sebevražd
- odchody spisovatelů do **emigrace** --> exilová literatura


## Oficiální

## Exilová
- Mnichov polovina 70. let
- nakladatelství ***Sixty eight publishers***
- exilové časopisy:
	- *Svědectví*
	- *Vokno*
	- *Revolver revue*

## [Samizdatová](/Samizdatova_literatura.md)
- vydávaná tajně v malých nákladech
- L. Vaculík