# Literatura: 50. léta - současnost


## Česká literatura
- **nejproblematičtější období** - vykonstruované procesy (Milada Horáková, Zelená internacionála)
- režim - nejtužší období
- vlivy - **komunistický převrat**
- **skupina 42**
	- skupina umělců
	- vznikla během okupace
	- [Josef Kainar](https://cs.wikipedia.org/wiki/Josef_Kainar), Jiří Kolář, Ivan Blatný
	- 1948 - konec činnosti - emigrace
- **"Květňáci"**
	- druhá polovina 50. let
	- časopis *Květen* (1955 - 1959)
	- poezie všedního dne - prostá, založená na faktech
	- Miroslav Holub, Miroslav Florian
- **časopis *Host do domu***
	- 60. léta
	- **Jan Skácel**
		- "básník Moravy"
		- motivy: příroda(=krása,život), plynoucí čas, ticho


[Václav Hrabě](/Autori/Vaclav_Hrabe.md) ?

Následuje [Literatura 1968 - 1989](Literatura_1968-1989.md)