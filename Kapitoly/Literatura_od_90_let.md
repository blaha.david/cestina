# Literatura od 90. let

## Michal Viewegh
- 1962
- smysl pro ironicko-groteskní tón
- ***Báječná léta pod psa*** (1992)
	- satiricky laděná rodinná kronika
	- vypravěč je nadprůměrně inteligentní Kvido, jehož rodiče se uchylují před normalizačními politickými tlaky na Sázavu
- ***Výchova dívek v Čechách*** (1994)
	- skutečné příběhy ze své učitelské praxe z braslavské školy
- ***Účastníci zájezdu*** (1996)
	- hodně autobiografie

## Petr Šabach
- humoristické povídky a romány --> autobiografie, hovorovost, nespisovnost
- vypravěč je vždy mladý kluk
- ***Hovno hoří*** (1994) + ***Jak potopit Austrálii*** (1986) -> námět pro Pelíšky


--------------------

- Tereza Boučková - *Rok kohouta*
- Irena Dousková - *Hrdý Boudžez*; *Oněgin byl Rusák*
- Jáchym Topol - *Sestra*; *Anděl*
- Květa Legátová - *Želary, Jozova Hanule*
- Kateřina Tučková - *Žítkovské bohyně*; *Vyhnání Gerty Snirch*
- Hana Mornštajnová - *Hana, Listopad*
- Patrik Hartl - *Prvok, Šampón, Tečka a Karel*; *Malý pražský erotikon*

