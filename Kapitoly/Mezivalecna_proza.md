# 18. Česká próza od konce 1. světové války do konce 2. světové války
- Jaroslav Hašek


## [Demokratický proud](/Proudy/Demokraticky_proud.md)
- Karel Čapek
- Karel Poláček
- Eduard Bass
- Josef Čapek



### [Německý píšící autoři](/Autori/Nemecky_pisici_autori.md)

### [Imaginativní proud](/Proudy/Imaginativni_proud.md)

### [Katolický proud](/Proudy/Katolicky_Proud.md)

### [Divadlo D 34](/Divadlo_D_34.md)

### [Osvobozené divadlo](/Osvobozene_divadlo.md)
- Meziválečné drama
- Jiří Voskocec, Jan Werich
