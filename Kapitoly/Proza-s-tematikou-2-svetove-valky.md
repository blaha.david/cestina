# Próza s tematikou 2. světové války

3 Vlny

## Česká:

###  1. vlna
- bezprostředně po válce
- protiválečná
- Informování/upozornění na chyby války
- **povídky** - **Jan Drda** - *Němá Barikáda*
- deníky, záznamy, dokumentární, svědectví - **reportáž**
- **Julius Fučík**
	- komunisti ho využívali v propagandě
	- vzor socialistického člověka
	- byl hezký - měl hezký obličej
	- byl popraven
	- ***Reportáž psaná na oprátce*** = "vězeňská literatura"
		- kontroverzní reportáž
		- část vynechána
		- popisuje celu, popisuje dozorce
			- jeden dozorce mu dával kusy papíru na které psal a vypouštěl je ven jeho ženě
		

### 2. vlna
- 60./70. léta - už delší dobu po válce
- židovská tematika - úděl Židů, holokaust
- **propracovanější** psychologie postav (než 1.vlna)
- **[Škvorecký](https://cs.wikipedia.org/wiki/Josef_%C5%A0kvoreck%C3%BD)**
	- *Zbabělci*, *Tankový prapor*
	- Autobiografie - hlavní postava je autor - **Danny Smiřický**
		- *paláce se jmenují po důležitých šlechtických rodech*
		- zakázané věci
- **Jan Otčenášek** - *Romeo, Julie a tma*
	- tragický motiv shakespearovské lásky
	- tma = ta doba = okupace
- **Arnošt Lustig**
	- koncentrační tábor - povedlo se mu uniknout
	- *Modlitba pro Kateřinu Horovitzovou* (1964) ([wiki](https://cs.wikipedia.org/wiki/Modlitba_pro_Kate%C5%99inu_Horovitzovou)) ([rozbor](https://www.cesky-jazyk.cz/ctenarsky-denik/arnost-lustig/modlitba-pro-katerinu-horovitzovou-rozbor.html))
		- psychologická novela
		- ze skutečné události jak unikla smrti útěkem z transportu do koncentračního tábora
		- děj se odehrává v r. 1943
- **Ladislav Fuks**
	- hodně temné, ponuré romány
	- byl psychicky nemocný
	- potlačovaná homosexualita
	- *Pan Theodor Mundstock*
	- *Spalovač mrtvol*

### 3. vlna
- 80. léta - současnost
- autoři kteří válku prožili jako děti
- velký odstup
- **[Bohumil Hrabal](/Autori/Bohumil_Hrabal.md)** - *Ostře sledované vlaky*
	- německé vlaky s municí (pouze projíždějí stanicemi)
	- lidový vypravěč - fenomenální paměť
	- sepisoval výslechy historek
	- psal na stroji - psal všema deseti a občas si dal ruku vedle a psal nesmysly a jeho korektor je musel opravovat
- **[Josef Škvorecký](/Autori/Josef_Skvorecky.md)** - *Zbabělci*, *Prima sezóna*
	- snažil se proslavit jako hrdina
- **Ota Pavel** - *Smrt krásných srnců*, *Jak jsem potkal ryby*
- **[Pavel Kohout](/Autori/Pavel_Kohout.md)** - *Hodina tance a lásky*



## Světová

### Východní blok
- ideologicky zaměřené/upravené
- větší důraz na hrdinství, neomylnost, velké bitvy, vlastenectví, statečnost osvoboditelů
- černobílé vidění války - my dobří, oni zlí
- pompézní scény - popis hromadného vraždění
- Jurij Bondarev - *Hořící sníh*
- [Michail Šolochov](https://cs.wikipedia.org/wiki/Michail_%C5%A0olochov) - *Osud člověka*
	- nobelova cena za literaturu - celoživotní čtyřdílné dílo *[Tichý don](https://cs.wikipedia.org/wiki/Tich%C3%BD_Don)*
- Vladimir Vojnovič - *Život a neobyčejná dobrodružství vojáka Ivana Čonkina*

### Západní blok
- nový naturalističtější pohled
- ukazuje zla války (často s nadsázkou)
- šedé vidění války - obě strany byly dobré i zlé
- VŠECHNO SOUVISÍ SE VŠÍM!!!!!!!!!!!
- Wiliiam Styron - *Sophiina volba*
- *Deník Anny Frankové*
- Thomas Kenally - *Shindlerův seznam*
- Patrick Ryan - *Jak jsem vyhrál válku*

Následuje [Literatura 50. léta](Literatura_50_leta.md)