# Světová literatura 2. poloviny 20. století

- značně mnohotvárná; mnoho témat i žánrů

1. Reakce na válku

	- rozdíl mezi východními a západními spisovateli ve zpracování tématu
	- východní – silný patos, pohled na válku – politicky uvědomělý; válečná vyprávění – soustředění na líčení vrcholných a pompézních scén (např. velké bitvy); mnohdy černobílé vidění hrdinů
	- západní – střízlivé a nepatetické líčení – daleko působivější; hrdinové – někdy i nehrdinové – strach, degradace člověka z hrůz války, soustředění se na psychiku – daleko sugestivnější a opravdovější pojetí; lit. – naturalističtější a dramatičtější

	- **př.**
		- Jurij Bondarev – Hořící sníh – líčení bitvy u Stalingradu – patetické
		- Michail Šolochov – Osud člověka – v popředí morálně-společenské problémy
		- Vladimír Vojnovič – Život a neobyčejná dobrodružství vojáka Ivana Čonkina (film. přepis – J. Menzel) – forma grotesky – jiný pohled na válku – antihrdina
		- William Styron – Sophiina volba
		- Deník Anny Frankové
		- Thomas Keneally – Schindlerův seznam
		- Patrick Ryan – Jak jsem vyhrál válku
		- Joseph Heller – Hlava XXII
		- Winston Churchil – Druhá světová válka (lit. faktu)
		- [E. M. Remarque](/Autori/Erich_Maria_Remarque.md) – Jiskra života; Čas žít, čas umírat
		- Pierre Boulle \[bul\] – Most přes řeku Kwai
		- William Wharton – Hlídka v Ardenách
		- Antonio G. Iturbe – Osvětimská knihovnice
	- k nejtragičtějším událostem dějin II. světové války – Hirošima; Osvětim – velikost zločinů, velikost lid. selhání dosáhla gigantických rozměrů → spisovatelé k tématu → stálé návraty → lidstvo dosud nepoučeno

1. Existencionalismus

	- ve 40. letech patří k nejvlivnějším proudům
	- e. zachycují krizi měšťácké společnosti a její dopad na jedince – izolovaného, plného úzkosti (v mezních situacích prožívá stav beznaděje), zoufalství, hnusu; člověk – osamocen, opuštěn Bohem
	- jednou z možností pro čl. → svoboda – uvědomuje si ji až ve vyhrocených, krajních životních situacích

	- **př.**

		- Albert Camus (Nobel. cena za literaturu) – Cizinec; Mor

		- Jean – Paul Sartre (Nobel. cena – odmítl ji přijmout → odmítal oficiální vyznamenání) – Hnus (román); Zeď (novela)

2. **Rozhněvaní mladí muži**

	- 50. – 60. léta v anglické literatuře

	- není to literární termín – vymysleli novináři

	- nesouhlas se společností blahobytu, konzumu, tradičních britských pořádků, konzervat. zvyklostí a morálky

	- díla s ostrou, štiplavou sociální kritikou; kromě protestu i zvláštní typ humoru; otevřenost a upřímnost

	- jazyk ulice, úřadů, moderní doby

	- ačkoli hrdina nejprve kritikem společnosti, často se do ní později výhodným sňatkem začleňuje

	- vznik nového žánru → univerzitní román – reakce na rozvoj univerzit. života v Anglii a v Evropě; rozvoj humanitních věd a intelektualizace společnosti

	- **př.**
	
		- David Lodge – Hostující profesoři

3. **Beatnici**

	- z angl. beat generation – zbitá generace – 50. a 60. léta v americké lit.

	- brání se vlivům spol. – nechtějí se stát jejími členy

	- výstřednost – šokující móda x nebo nepečují o svůj zevnějšek

	- tuláctví, džez, sklon k alkoholismu, drogy, sexuální nevázanost, sklon k praktikám Dálného východu – zenbudhismus

	- jazyk ulice, vulgarismy, erotická otevřenost, téma nového životního stylu, neskrývaná homosexualita

	- **př.**

		- Allen Ginsberg – sb. Kvílení → základ bud. undergroundu 60. let

		- Jack Kerouac – Na cestě

		- William Burroughs – Feťák; Teplouš

		- Charles Bukowski – Všechny řitě světa i ta má; Erekce, ejakulace a jiné povídky

4. **Neorealisté**

	- nejvýz. umělec. směr po 2. sv. válce v Itálii – největší rozmach – 40. – 50. léta

	- spojen s kinematografií (Federico Fellini – režisér, scénárista)

	- důraz na dokumentárnost, hodnověrnost + kritičnost zobrazování

	- pozornost sociální a mravní problematice – nezaměstnanost, bída, chudoba, prostituce, zaostalost

	- neoralist. filmy a texty – dodneška fascinují syrovou realitou, důrazem na věcnost

	- **př.**

		- Alberto Moravia – Horalka; Římanka

5. **Postmodernismus**

	- z USA – souvisí s vývojem architektury → proti modernosti staveb, z nichž se vytratila skutečnost, že stavba slouží člověku

	- 60. léta

	- v románu – složka epická i meditativní → román současně – detektivkou, historickým románem, filozofickou úvahou, sociologickou sondou

	- **př.**

		- Umberto Eco – Jméno růže

		- Vladimir Nabokov – Lolita

		- filmy Woodyho Allena

6. **Magický realismus**

	- typický pro země se silnou tradicí mytologie či ústní slovesnosti (Latinská Amerika – Kolumbie, Argentina, Kuba, Brazílie)

	- myt. Výklad ovlivňuje vyprávění autora

	- zákl. principem → prolínání dvou světů – reálného a fantazijního

	- během vyprávění → těžko postřehnutelná hranice mezi oběma světy

	- **př.**

		- Gabriel García Márquez (Nobelova cena) – Sto roků samoty; Kronika ohlášené smrti; Láska za časů cholery

		- \+

		- Haruki Murakami (japonský autor) – Kafka na pobřeží; Norské dřevo

7. **Psychologická próza**

	- Jerome David Salinger – Kdo chytá v žitě – o problematice dospívání John Irving – Pravidla moštárny; Svět podle Garpa; Modlitba za Owena Meanyho

8. **Sci-fi a fantasy literatura**

	- Georg Orwell – 1984; Farma zvířat

	- Ray Bradbury – Marťanská kronika; 451 stupňů Fahrenheita

	- Arthur C. Clarke – Vesmírná odyssea

	- Isaac Asimov – Já, robot

	- Stanislav Lem – Astronauti

	- John Wydham – Den trifidů

	- [J. R. R. Tolkien](/Autori/J_R_R_Tolkien.md) – Hobit; Pán prstenů

	- Robert E. Howard – tvůrce Conana (otec žánru fantasy)

	- Terry Pratchet – Zeměplocha

9. **Kriminální román**

	- př.
		- Jo Nesbo – Spasitel

	- Stieg Larsson – Milénium (Muži, kteří nenávidí ženy)