## Renesance a Humanismus 
- 14\. až 16. století
- z francouzského *renaissance* = **znovuzrození**, obnovení
- snaha obrodit člověka za pomoci obnovení kultury antických Řeků a Římanů
- poznání člověka, obrat od boha k člověku
- návrat k antice (Řím a Řecko)
- staví se proti nadvládě boží existence
- prosazuje rozvoj přírodních věd a věd o člověku
- prosazuje uvědomělý individualismus = **důraz na osobnost člověka**
- knihtisk (Johannes Gutenberg) - rozvoj literatury
- humanismus je ideál vzdělání, *NENÍ* to literární směr
- autoři
	- Itálie
		- Dante Alighieri - Božská komedie
		- Giovanni Boccaccio - Dekameron
	- Francie
		- Francois Villon - Malý testament, Velký testament
		- Pierre de Ronsard - Milostné poezie
	- Španělsko
		- Miguel de Cervantes y Saavedra - Důmyslný rytíř Don Quijote de la Mancha
		- Lope de Vega - Ovčí pramen
	- Anglie
		- [William Shakespeare](/Autori/William_Shakespeare.md) - Hamlet, Romeo a Julie
		- Geoffrey Chaucer - Canenburské povídky
	- Česko
		- Jan Blahoslav - Filipika proti misomusům, Bible kralická


-----------------
--> [2. Baroko](/Literarni_smery/02_Baroko.md)