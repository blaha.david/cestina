## Baroko
- 2\. polovina 16. století až 80. léta 18. století
- období 30. leté války
- vzrůstá vliv církve - upřednostnění víry nad rozumem
- v Česku rozdělena na 2 pod-směry
	- domácí
		- duchovní, katolická
		- většinou jezuité
	- exulantská
		- nekatolící, protestanti
		- v exilu (exulantská), např. Jan Ámos Komenský


-----------------
--> [3. Klasicismus](/Literarni_smery/03_Klasicismus.md)