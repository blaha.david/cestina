## Klasicismus
- 2\. polovina 17. století až 18. století
- z latiny *classicus* = vzorný, vynikající
- vzorem je antika
- vznik ve **Francii** v **17. století** v době rozkětu absolutistické monarchie
- pevná pravidla (například trojjednota - čas, děj, místo)
- upřednostňuje rozum, vyzdvihuje nutnost řádu, morálky
- **zaměření na společenské a mravní problémy**
- snaha o zobrazení "člověka vůbec"
- různé lidské charaktery pojaté nadčasově - hrdina, padouch, lakomec, ...


### Stavby
- korintské sloupy
- trojúhelníkový štít
- symetrie
- zámky - francouzské zahrady



-----------------
--> [4. Osvícenství](/Literarni_smery/04_Osvicenstvi.md)