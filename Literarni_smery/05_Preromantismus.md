## Preromantismus (též sentimentalismus)
- od poloviny 18. století do romantismu
- přechod mezi klasicismem a romantismem
- důraz na cit, odmítání konvencí
- návrat k přírodě, vzor venkova a prostosti

-----------------
--> [6. Národní obrození](/Literarni_smery/06_Narodni_obrozeni.md)