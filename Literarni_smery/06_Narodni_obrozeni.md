## Národní obrození
-  = proces utváření novodobého národa
- 70\. léta 18. století až 50. léta 19. století
	- kříží se s preromantismem
- reakce na poněmčování, germanizaci
	- v rámci habsburské monarchie
	- němčina jediný úřední jazyk, čeština vytlačena ze škol
	- zrušení nevolnictví - česky mluvící lidé přicházejí do poněmčených měst
- zájem o český jazyk, rozvoj divadla

### 1. fáze obranná
- snaha o zrovnoprávnění čechů v habskurské monarchii
- zaměření na slavnou minulost, nářek nad stavem jazyka a národa
- autoři
	- **Josef Dobrovský** - Zevrubná mluvnice jazyka českého, Dějiny českého jazyka a literatury
	- Martin Pelcl - Nová kronika česká
	- Václav Klicpera - Divotvorný klobouk
- rozmach divadla jako způsob dostat češtinu a vlastenecké myšlenky mezi masy
	- Bouda
		- 1786 na Koňském trhu (Václavské náměstí)
		- první česká divadelní scéna

### 2. fáze ofenzivní
- důraz na jazyk, rozvinutí češtiny
- zájem o historii (občas nedoložené prameny, falzifikáty)
- autoři
	- **Josef Jungmann** - Slovník česko-německý, Slovesnost aneb Sbírka příkladu s krátkým pojednáním o slohu
	- František Palacký - Dějiny národu českého v Čechách i v Moravě
	- Jan Kollár - Slávy dcera
	- František Ladislav Čelakovský - Ohlas písní českých, Ohlas písní ruských
- rukopisy
	- touha mít národní zásadní dílo, po vzoru ostatních národů
	- Rukopis královehradecký
	- Rukopis zelenohorský
	- studie T. G. Masaryka odhalila oba za falzifikáty

### 3. fáze vyvrcholení
- úkolem literatury je povzbuzovat národní sebevědomí a vychovávat k vlastenectví
- objevují se prvky romantismus (Karel Hynek Mácha)
- autoři
	- Karel Jaromír Erben - Kytice
	- [Karel Hynek Mácha](Autori/Karel_Hynek_Macha.md) - Máj

### 4. fáze dovršení
- objevují se prvky realismu
- autoři
	- [Karel Havlíček Borovský](/Autori/Karel_Havlicek_Borovsky.md) - Obrazy z Rus, Tyrolské elegie, Král Lávra
	- [Božena Němcová](/Autori/Bozena_Nemcova.md) - Babička




-----------------
--> [7. Romantismus](/Literarni_smery/07_Romantismus.md)