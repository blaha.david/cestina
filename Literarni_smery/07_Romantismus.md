## Romantismus
- 1\. polovina 19. století
- důraz na cit a fantazii
- protiklad klasicismu
- vyjímečný hrdina
- rozpor mezi představou a skutečností
- autoři
	- Německo
		- bratři Grimmové - německé lidové povídky
		- Heinrich Heine - Kniha písní
	- Anglie
		- George G. Byron - Childe Haroldova pouť
		- Victor Hugo - Chrám Matky Boží v Paříži
		- Stendhal - Červený a černý
		- Walter Scott - Ivanhoe
	- USA
		- [Edgar Allan Poe](/Autori/Edgar_Allan_Poe.md) - Jáma a kyvadlo
	- Rusko
		- Alexandr S. Puškin - Evžen Oněgin
		- Michail Lermontov - Démon
	- Česko
		- Karel Hynek Mácha - Máj



-----------------
--> [8. Realismus a Naturalismus](/Literarni_smery/08_Realismus_a_Naturalismus.md)