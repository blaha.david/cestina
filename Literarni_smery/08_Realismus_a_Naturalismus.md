## Realismus a Naturalismus
- 2\. polovina 19. století
- realismus
	- pravdivý obraz skutečnosti
	- hrdina i děj se vyvíjí
	- kritika nedostatků společnosti
- naturalismus
	- podobný realismu
	- z teorií o silné přírododní stránce člověka (determinismus)
	- popření lidské vůle
	- hrdinové z nejnižších vrstev


-----------------
--> [9. České literatura 2. poloviny 19 století](/Literarni_smery/09_Ceska_literatura_2_poloviny_19_stoleti.md)