## Česká literatura 2. poloviny 19. století
- volné zacházení s žánry
- kontrastní prvky
- Poznámka
	- Májovci jsou první, Lumírovci a Ruchovci nastupují až po nich
	- Lumírovci a Ruchovci měli opačné hodnoty, soupeřili spolu
- Májovci
	- almanach Máj (odkaz Karla Hynka Máchy)
	- autoři
		- [Jan Neruda](/Autori/Jan_Neruda.md) - Povídky Malostranské
		- Vítězslav Hálek - Večerní písně
- Ruchovci
	- almanach Ruch (Václav Ruch)
		- vznik při položení základního kamene Národního divadla
	- volná návaznost na Májovce
		- chtěli odprostění české literatury od cizích vlivů
		- tradiční názory, národní dějiny 
	- autoři
		- Svatopluk Čech - Výlety pana Broučka
		- Eliška Krásnohorská - Z máje žití
- Lumírovci
	- časopis Lumír (Josef Lumír)
	- volná návaznost na Májovce
		- chtěli světovost české literatury, svobodnou básnickou tvorbu
	- autoři
		- Jaroslav Vrchlický - Noc na Karlštejně
		- Julius Zeyer - Vyšehrad, Radůz a Mahulena
- Realismus v české próze 80.-90. let 19. století
	- navazují na realismus 40. let
		- na Němcovou, Havlíčka apod.
	- za účelem rozvoje národa a emancipace
	- autoři
		- Alois Jirásek - Staré pověsti české 
		- [Ladislav Stroupežnický](/Autori/Ladislav_Stroupeznicky) - Naši furianti



-----------------
--> [10. Literární moderna](/Literarni_smery/10_Literarni_moderna.md)