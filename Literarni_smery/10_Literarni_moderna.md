## Literární moderna
- Impresionismus
	- lyričnost a hudebnost verše
	- emoce a nálady
- Dekadence
	- "úpadek"
	- pocity marnosti, prázdnoty, zklamání
- Symbolismus
	- rozvoj básnické obraznosti, hudebnosti
	- zobrazování věcí které nejdou racionálně popsat
- autoři
	- Francie (prokletí básníci)
		- Paul Verlaine - Moudrost (symbolista)
		- Charles Baudelaire - Květy zla (symbolista)
		- Arthur Rimbaud - Iluminace (dekadent a symbolista)
	- Anglie
		- Oscar Wilde - Obraz Doriana Graye (dekadent)
	- Česko (tzv. Česká Moderna)
		- generace autorů spjatá s manifestem České moderny
		- Antonín Sova - Zlomená duše (symbolista, impresionista)
		- Karel Hlaváček - Pozdě k ránu (symbolista, dekadent)
		- Otakar Březina - Tajemné dálky (symbolista)
- [Anarchističtí buřiči](/Anarchisticti_burici.md)
	- zabývali se aktuálními problémy
	- odpor proti státu, většinou anarchisté, bohémský život
	- autoři
		- Petr Bezruč - Slezské písně
		- Viktor Dyk - Krysař
		- Stanislav Kostka Neumann - Rudé zpěvy
		- Fráňa Šrámek - Života bído
		- František Gellner - Radosti života
		- Karel Toman - Pohádky krve



-----------------
--> [11. Literatura 1. poloviny 20. století](/Literarni_smery/11_Literatura_1_poloviny_20_stoleti.md)