# Májovci
- 60\. léta 19. století
- umělci kolem almanachu Máj - sympatie k Máchovi
- chtěli navázat na Máchův odkaz
- *Mácha byl nadčasový, bylo mu to vyčítáno*
- svou tvorbou bojovali proti sociálnímu a národnostnímu útisku
- lidovost – tvorba pro lid a o něm
- úsilí povznést českou literaturu na světovou úroveň

## [Jan Neruda](/Autori/Jan_Neruda.md)

## Karolína Světlá (Johana Rottová), (1830–1899)
- vl. jménem Johanna Rottová
- 1853 jí zemřela dcera, odjela do rodiště manžela Světlé v Podještědí (odtud pseudonym)
- zakladatelka českého vesnického románu
- ještědské romány: *Kříž u potoka, Vesnický román, Frantina*

## Vítězslav Hálek (1835–1874)
- harmonické manželství – oženil se s dcerou bohatého advokáta
- ve 39 letech onemocněl zánětem pohrudnice, brzy zemřel
- optimistická, pohodová, smířlivá tvorba = idyličnost
- čtenářsky velmi oblíbený – písňový tón, snadná zapamatovatelnost
- *Večerní písně, Muzikantská Liduška, Na vejminku*

## Jakub Arbes (1840–1914)
- redaktor Národních listů, musel odejít 
- literární kritik – Hugo, Poe, Zola, Mácha
- první významný český spisovatel s technickým vzděláním (nedokončeným)
- zájem o jevy, které se nedaly vysvětlit
- --> nový literární žánr **romaneto**: text menšího rozsahu s napínavým dějem, v němž se řeší tajemná záhada (to, co vypadá jako nadpřirozený jev, je vysvětleno vědeckým výkladem)
- *Svatý Xaverius, Newtonův mozek, Etiopská lilie*

## Adolf Heyduk (1835–1923)
- *Karyatidy, Z rodných hor, Lotyšské motivy*
