# Funkční styly

- ### [Prostěsdělovací styl](https://cs.wikipedia.org/wiki/Prost%C4%9Bsd%C4%9Blovac%C3%AD_styl)
	- styl běžné každodenní komunikace
	- hovorový
	- např. v rodině, mezi přáteli, ve škole, v zaměstnání apod.
- ### [Odborný styl](https://cs.wikipedia.org/wiki/Odborn%C3%BD_styl)
	- styl odborných publikací, odborných časopisů a učebnic.
	- Jeho cílem je podat přesné, jasné a relativně úplné informace z různých oborů lidské činnosti a poučit tím adresáta. 
	- Často se také používají odborné pojmy.
- ### [Publicistický styl](https://cs.wikipedia.org/wiki/Publicistick%C3%BD_styl)
	- Styl médií.
	- Vyznačuje se spisovným jazykem, je proměnlivý a dynamický.
	- Podává nové aktuální informace.
- ### [Umělecký styl](https://cs.wikipedia.org/wiki/Um%C4%9Bleck%C3%BD_styl)
	- styl spisovatelů
	- zaměřuje se na funkci poznávací, estetickou a osobitost vyjádření.
- ### [Administrativní styl](https://cs.wikipedia.org/wiki/Administrativn%C3%AD_styl)
	- Styl informativní a věcné komunikace.
	- Někdy se též označuje jako jednací či úřední.
	- Je zaměřen především na fakta a na jejich sdělení.
- ### [Řečnický styl](https://cs.wikipedia.org/wiki/%C5%98e%C4%8Dnick%C3%BD_styl)
	- Styl veřejných a oficiálních mluvených projevů.
	- Plní funkci přesvědčovací a propagační.




### Kaligram (vizuální poezie)
 - propojení písma s výtvarným uměním
 - básně se skládají do tvaru téma básně
 - lettrismus - umělecký směr

eduard bass
vančura

balada pro banditu

proletářská poezie
wolker
špatné pracovní podmínky
víra v revoluci, která to změní
sbírka těžká hodina
sociální balady
o snu
