
**invence** - vtipný nápad, důvtip, vynalézavost
**kompozice** - uspořádání jazykových složek, tvoření slov skládáním
**stylizace** - úprava, přizpůsobení
**korektura** - oprava/náprava chyb v textu

# Slohotvorní činitelé:

= podmínky, za nichž vzniká jazykový projev

### Subjektivní - jsou vždy spojeni s původcem projevu:
Jsou podmíněni **osobností autora.**

Jeho **pohlavím, věkem, vzděláním** , jeho výrazovou schopností, zájmy, jeho **stanoviskem k dané problematice**. Determinují jeho projev. Každý **má svůj individuální styl** , který závisí právě na subjektivních slohotvorných činitelích. **Projevuje se zde charakter, intelekt autora, vtipnost** , atd.

### Objektivní - nejsou závislí na osobě tvůrce textu:
**Obecně platné věci, ke kterým autor musí přihlížet a je jimi vázán**. Je to například **zaměření projevu, cíl na který chce poukázat, zda je projev připravený nebo nepřipravený**. Musí přihlížet k tomu, jestli existuje **přímý kontakt mezi mluvčím a adresátem** (má možnost přizpůsobit se jejich reakci, důraz na sílu hlasu a intonaci) nebo je adresát nepřítomný. Záleží na tom, jestli chce vést **monolog nebo dialog** , jestli prostředí k jednání je **oficiální nebo soukromé** , jestli chce mluvit **k dětem, odborníkům nebo studentům** , jestli je projev **písemný nebo mluvený** , atd.

[https://studentka.sms.cz/index.php?P\_id\_kategorie=7630&amp;P\_soubor=%2Fstudent%2Findex.php%3Fakce%3Dprehled%26ptyp%3D%26cat%3D15%26idp%3D%26detail%3D1%26id%3D3639%26view%3D1%26url\_back%3D](https://studentka.sms.cz/index.php?P_id_kategorie=7630&amp;P_soubor=%2Fstudent%2Findex.php%3Fakce%3Dprehled%26ptyp%3D%26cat%3D15%26idp%3D%26detail%3D1%26id%3D3639%26view%3D1%26url_back%3D)

