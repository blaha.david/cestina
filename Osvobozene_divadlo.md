# Osvobozené divadlo


= **autorské divadlo** = herci jsou autoři her

[Prezentace Osvobozené divadlo](https://docs.google.com/presentation/d/12CVFaCKoV_sUiTxs71Q8NSij4NUcD-0LgCFYsPC21Wc/edit?usp=sharing)

Ježek - téměř slepý
slyší barvy
modrá

**forbíny = předscény**

Voskovec a Werich dělali inteligentní humor

50\. léta byla krutá



-   1925 (1926)
-   pražská *avantgardní* divadelní scéna v sekci *Devětsilu*
-   Režiséři Jiří Frejka a Jindřich Honzl
-   žižkovská Zkušební scéna (1923)
-   Divadlo Na slupi
-   Ovlivněno dadaismem, futurismem a poetismem
-   Kritika společnosti
-   Důraz na fantazii herců i diváků, aktivní spoluúčast
-   Snaha o překonání odstupu mezi jevištěm a hledištěm - dialogy herců s obecenstvem
-   od r. 1927 divadélko v Umělecké besedě na Malé Straně
-   Od května r. 1927 - Jiří Voskovec a Jan Werich 
-   První představení: *Vest pocket revue* *(Malá kapesní reví)*
	-   Uvolněný dramatický žánr plný humoru (parodoval tradiční drama a operu)
-   Od září 1928 v hotelu Adria na Václavském nám.


## Hry:
-   Vest pocket revue
-   Caesar
-   Osel a stín
-   Kat a blázen
-   Rub a líc - zfilmováno jako Hej rup!
-   Těžká Barbora
-   Pěst na oko


#Avantgarda: umělecké hnutí první poloviny 20. století. Poslední vývojová fáze novodobého umění. Přechází jí moderna. Antitradicionalismus a společenská revolta.

#Devětsil: levicový umělecký svaz (1920-1930)
Volné sdružení posluchačů dramatické konzervatoře pražské, hrající s laskavým svolením rektorátu
