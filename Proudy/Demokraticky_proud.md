# Demokratický proud

## = "Autoři z okruhu Lidových novin"

- také *pragmatisté*
- 20\. léta 20. století
- demokracie
- proti ohrožování svobody názorů
- pragmatismus
- lidové novin
- proti válce a rasismu
- Humanistická literatura
	- zajímá se o prostého člověka
	- vlastnosti
	- život

## Eduard Bass
- Eduard Shmidt
- básně v **kabaretu** - fejetony
	- autor, skladatel
	- např.: kabaret "Červená sedma"
- šéfredaktor Lidových novin
	- **"soudničky"**
		- = příběh ze soudní síně zpracovaný vtipnout formou
		- výše trestu - za slovní urážku dostala 2 roky vězení?
	- **"rozhlásky"**
		- Bass jednou zveršoval týdenní zprávy
		- vyšli v příloze Lidových novin
		- velmi úspěšná forma
- renesanční člověk?
- *Klapzubova jedenáctka*
	- rodina Klapzubů
	- založili si fotbalový tým
	- dvakrát zfilmována
	- Děj:
		- 11 synů?
		- fotbalové mužstvo
		- dobývá svět
		- zvítězí nad Barcelonou
- *Cirkus Humberto*
	- složitá četba
	- hodně dějových linií
	- hudba Karla Svobody
	- Děj:
		- o Vaškovi, který se se svým tátou dostal k cirkusu
		- mapuje život Vaška, který se později stal majitelem toho cirkusu
		- hospodářská krize



## [Karel Poláček](/Autori/Karel_Polacek.md)
- Lidové noviny - fejetony
- **Autor *Sloupku***
- inspirační zdroj = město **Rychnov nad Kněžnou**
- miloval židovské anekdoty
- Vypravěčský styl - spojování spisovné a nespisovné češtiny
- Terezín, později transport do Osvětimi
- *Muži v ofsajdu*
- *Bylo nás pět*
	- život na malém městě
	- pohled menších dětí
	- jejich dobrodružství
	- Péťa Bajza
- *Hostinec u kamenného stolu*
	



## Josef Čapek
- starší bratr Karla
- Lidové noviny, Národní listy
- skupina výtvarných umělců
- malíř - kubismus
- zemřel v koncentračním táboře
- *Povídání o pejskovi a kočičce*
- *Stín kapradiny*


## [Karel Čapek](/Autori/Karel_Capek.md)
- Bechtěrevova nemoc - nebyl v armádě
- Lidové noviny, Národní listy
- *Válka s mloky*
- *Bílá nemoc*
- *RUR*
	- strach z technologií
