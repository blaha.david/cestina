# Imaginativní proud
= "básnický proud"

[ČJL - Imaginativní proud - Vladislav Vančura](https://docs.google.com/presentation/d/1GQcZeDTS2_MZmbBk5tRING-k8Xo8uGfJO_kNRXmfaQQ/edit#slide=id.gf4fe246fe9_0_32)

- próza, ale jazykem připomíná poezii
	- básnická vyjádření
	- obsahuje zvukomalba
	- není členěna do veršů
	- nemá rým


## [Vladislav Vančura](/Autori/Vladislav_Vancura.md)