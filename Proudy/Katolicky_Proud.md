# Katolický proud
## Katolická poezie

## Charakteristika
- duchovní a křesťanská orientace
- kritika režimu
- po 1948 - problémy s publickováním
	- ***"proces se zelenou internacionálou"***
	- souzení v politických procesech
	- vězení, trest smrti, zákaz publikování


## Představitelé
- **Jakub Deml**
- **Jaroslav Durych**
- Jan Čep
	- *Dvojí domov*
- Jan Zahradníček
	- vyškrtnut z učebnic
	- 13 let vězení - odsouzen za velezradu (kterou nespáchal)
	- básnická sbírka *Znamení moci*
- Bohuslav Reynek
	- *Smutek země*, *Sníh na zápraží*


### Jaroslav Durych
- Pseudonym Jaroslav Žabka
- 1886 - 1962
- studoval na gymnáziu v Hradci Králové
	- vyhodili ho
- vojenský lékař za WW1
- velký nacionalista
- Kritika Karla Čapka
	- za pacifismus
	- za to, že nesloužil v povinné vojenské službě
- **Díla**:
	- básnická sbírka *Pláč civilisty*
		- kritika Karla Čapka
	- *Boží duha*


### Jakub Deml
- Tasov (Morava) 1878 – 1961 Třebíč 
- Spisovatel a kněz
- dílo *Mé svědectví o Otokaru Březinovi*
	- Dobrý přítel, bořil idealizace 
	- K. Čapek potvrdil
	- ostrá kritika díla
- **Díla:**
	- *Hrad smrti*
		- [surrealistické](/Umelecke_smery/Surrealismus.md) dílo
		- hlavní motiv: smrt
		- koncepce díla: není nijak seřazeno
	- *Moji přátelé*
		- pestrá barvitá poezie
		- naturalistický pohled na věc
		- květiny
		- idealizuje
- Konflikt s církví
	- Krátce kněz
	- Konstantní spory (kritika)
	- Katolická moderna – *Nový život*
		- Odvrat
- Konflikt s masarykem
	- Urážka Masaryka (3 citace Otokara Březiny) --> K. Čapek: Případ s Otokarem Březinou 
	1. Kritika literatury (Světová revoluce), Odpor k církvi
	3. Nemá ponětí, co říká
	4. Škola: propaganda, zlá
- Konflikt s novým režimem
	- *Šlépěje* --> Rusko vedeno tyranem
	- Nezval mu navrhl pomoc  
		- „Nikdo mi z toho nebude dělat guláš“
		- Dal mu peníze
