# Autoři levicového proudu

- 20. století
- Sodiální témata
- osudy dělníků
- život ve městech - jak je jiný porpti venkovu
- typické prostředí průmyslu
- měli blízko k leninismu
- poté členi komunistické strany
- ovlivněny levicovými sociálními myšlenkami
- vyvíjí se vedle demokratického proudu


## [Ivan Olbracht](/Autori/Ivan_Olbracht.md)
- **1882** - **1952**
- spisovatel, publicista, novinář, překladatel
- narozen v Semilech 
- psal to *Rudého Práva*
- ### Nikola Šuhaj loupežník
	- 1933
	- román
	- odehrává se po WWI.
	- Podkarpatská Rus
	- spojeny dvě události
		- skutečná - o válečném sběhovi
		- fiktivní příběh - 
	- ve 30. letech byl román vyřazen z knihoven
		- a proč jako třeba nechceš říct?
	- **Děj:**
		- Nikola s kamarádem se vrátí z vojny
			- jako váleční sběhové se musí ukrývat
			- pomáhají mu kamarádi
		- lidi jsou nespokojení - rabují
		- ostatním lidem se to nelíbí, jsou povonáni vojáci, aby situaci ustálil
		- Nikola - hrdina - bohatým bere, chudým dává
		- vypsána vysoká odměna na Nikolu
		- nakonec ho zradí přátelé a umírá
			- zabit sekerou svým přítelem a ještě do něho nastřílel několik ran, aby se zásluhy připsali četnictvu
		- pohřben bez náhrobku, bez rakve, ale jeho legenda žije dál vlesích
	- státní cena za literaturu v r. 1993
	- podobnost s Robinem Hoodem, Janošíkem
- ### Anna Proletářka
	- [proletářská literatura](/Umelecke_smery/Proletarska_poezie.md)
	- Olbracht píše o přechodu a ovlivnování
	- Anna přijede do Prahy
	- potká se s někým a ten ji přesvědčuje o správnosti levice


## Marie Majerová
- marie Bartošová
- česká prozaička, komunistická novinářka
- ocenění: národní umělec, Řád práce, Státní cena Klementa Gottwalda
- ### Siréna
	- román
	- rodina dělníků - Hudcovi
	- v prostředí průmyslového města Kladna
	- fabulovaný příběh propleten se skutečnými reáliemi a historickýmy okamžiky
	- 1947 zfilmován režisérem Karlem Steklým
- ### Robinzonka
	- o opuštěnosti
		- podobnost s Robinem Hoodem
	- zemřela jí matka
		- všechny matčiny povinnosti přešli na ni

docela ooc:
## Divadlo Brněnské - "husa na provázku"
### Balada pro banditu
- vynikající hra (hraná do dneška)
- *Zabili, zabili*
- Insporováno Nikola Šuhaj loupežníkek
- Podkarpatská Rus
- 

