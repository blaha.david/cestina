# Psychologická próza
- vnitřní charakterizace a motivace
- průzkum mentality postav
- důležité je **proč** něco postavy dělají
- vyjádření myšlenek - *proud vědomí, retroperspektiva*
- vyprávěcí prostředky - *dějové zápletky, nespolehlivý vypravěč*

Dostojevskij - zakladatel psychologické prózy

## 19. století
- rozmach psychologické prózy
- Stendhal - Červený a černý (1830)
- Dostojevskij - Zločin a trest (1866)
	- důraz na vnitřní prožívání postavy
- Hamsun - Hlad (1890), Mystérie
- S. Freud - Psychoanalýza


## V ČR
-   Především před 2. světovou válkou
-   **Jarmila Glazarová**
	-   Severní morava
	-   Málo postav v dílech, typicky žena jako hlavní postava
	-   Účast v komunistické straně
	-   Roky v kruhu (1936), Advent (1938), Vlčí jáma (1939)
-  ** Egon Hostovský**
	-   Osamělost a spiknutí proti člověku
	-   Cizinec hledá byt (1947), Všeobecné spiknutí (1961)
- ** Marie Pujmanová**
	-   Inspirovaná impresionismem a později socialismem
	-   Po roce 1933 kritizovala fašismus
	-   Trilogie Lidé na křižovatce (1937), Hra s ohněm (1948), 
		Život proti smrti (1953)
-   **Václav Řezáč**
	-   Originálně psal verše do časopisů a dětskou literaturu
	-   S nástupem válečných let začal psát psychologické romány
	-   Zaměřoval se na osídlování pohraničí po druhé světové válce
	-   Kluci, hurá za ním (1934, pro děti), Černé světlo (1940), Svědek (1943) 

##  Jaroslav Havlíček
- **Jeden z nejdůležitějších představitelů v ČR**
- lidé postiženi patologickými jevy společnosti
- *Petrolejové lampy* (1935)
	- naturalisticky vykreslený román
	- příběh nehezké ženy, která touží po citově naplněném životě,
	- toho se jí v manželství přes její dobrou povahu a hmotné zajištění nedostává
	- její manžel dostane schizofrenii
	- manžel ji fyzicky napadne a poté ho odvezou do psychiatrické léčebny
- *Neviditelný* (1937)
	- román
	- psychologická studie, popisující osudy rodu Hajnů, jemuž vládne šílený strýc postižený představou, že je neviditelný
	- rodovému „prokletí" neunikne ani z vypočítavosti a touhy po majetku přiženěný muž, který ztrácí svou ženu a zjišťuje, že také jeho syn je duševně postižený
- *Helimadoe* (1940)
	- název ze jmen pěti dcer
	- je ozvezen za venkovským lékařem
	- jeho dcery se střídají o služby


## Richard Weiner
-   1884-1937
-   Vystudoval pivovarní chemii
-   Pobýval ve Francii, byl jí ovlivněn ve své tvorbě, psal také recenze na francouzskou literaturu
-   Ovlivněn lidovou tvorbou, národním obrozením, českým symbolismem a Francouzskými umělci (Apollinaire, Le Grand Jeu)
-   Pseudonym Jan Bol; Štěpán Golev
-   Těžké zařadit jeho díla do směru, velice komplexní
-   Centrálním tématem je rozpolcenost člověka a nesrozumitelnost lidské existence
-  *Lítice, Škleb, Mnoho nocí*

## Ladislav Klíma
-   1878 Domažlice – 1928 Praha
-   Prozaik, dramatik, básník, především filozof
-   Filozofie založena na solipsismu a egodeismu
	-   *"Já jsem bůh"*
-   Velkou část svého díla sám spálil
-   Časté sci-fi prvky
-   *Utrpení knížete Sternenhocha*
	-   přeměna hezké paní na stvůru?
-   *Velký román*
	-   žijí v nepřístupném lese
	-   vlastní pravidla/zákony

* * *

### Psychologický horor

-   Snaha o vytvoření diskomfortu nebo zhrození odhalením nejčastějších psychologických zranitelností a strachů
-   Prožívání na základě vlastního děsu
-   Temnější stránky lidské mysli (podezření, paranoia atd)
-   Vyskytuje se nejistota o tom co je realita vnímáním děje z pohledu postavy
-   Zaměřené na mentální konflikty
-   Např. E.A. Poe, Meyerink – Golem, Stephen King, Ring

### Psychologický thriller
-   Podobné hororu, často se překrývají a některá díla mohou být zařazena do obou
-   Rozdílnost od hororu:
	-   Nezpůsobuje obavy o osud protagonisty, ale sama sebe
	-   Zdroj napětí je reálný, není vyvolán fiktivními jevy
-   Charakterizovány rychlým sledem událostí, častou akcí a často pokusy o přechytračení protagonisty/antagonisty


### Psychologické sci-fi
-   Psychologické drama nebo thriller v sci-fi prostředí
-   Často řeší problémy protagonisty s politikou nebo technikou 
-   Často filmy
	-   Např. *Mechanický pomeranč* (1971), *Konec Evangelionu* (1997), *Počátek* (2010)


### Psychologické drama
-   Spojené s psychologickým dramatem
-   Často se jedná o témata jako dospívání, opuštěnost, nefunkční vztahy, sebevražedné sklony (SSPŠ haha) nebo mentální problémy