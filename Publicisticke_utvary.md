# Publicistické útvary

Použití v médiích: tisk, televize, rozhlas
Spisovný jazyk

## Požadavky:
- aktuální
- srozumitelné
- přesvědčivé
- zajímavé
- (objektivní)


## Útvary:
- [Článek](/Sloh/Clanek.md) - měl by být objektivní
- [Reportáž](/Sloh/Reportaz.md) - (mezi uměleckým a publicistickým stylem)
- [Zpráva](/Sloh/Zprava.md) - krátký článek
- [Sloupek](/Sloh/Sloupek.md) - (mezi uměleckým a publicistickým stylem)
- [Fejeton](/Sloh/Fejeton.md)
