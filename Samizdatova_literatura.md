# Samizdatová literatura
- vydávaná tajně v malých nákladech
- vydávali a šířili si to sami
- opisované na stroji
- **[Ludvík Vaculík](https://cs.wikipedia.org/wiki/Ludv%C3%ADk_Vacul%C3%ADk)**
- "[Edice Petlice](https://cs.wikipedia.org/wiki/Petlice_(edice))"
- 2\. polovina 70. let