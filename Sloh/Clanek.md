# Článek
[https://whatsin.spst.cz/index.php/2019/04/06/maturitni-sloh-clanek/](https://whatsin.spst.cz/index.php/2019/04/06/maturitni-sloh-clanek/)
-   Článek musí obsahovat **nadpis** (nejlepší je vymyslet si vlastní, ale je možné použít nadpis ze zadání)!
-   Článek je útvar publicistického stylu. To znamená, že snažíme čtenáři přestavit nějakou událost/výrobek/trend/… Možnosti jsou v podstatě neomezené a musíme se přizpůsobit konkrétnímu zadání.
-   Strukturou by měla být „**obrácená pyramida**“ (nejprve uvedeme, o čem píšeme, pak jdeme do hloubky). Nezapomeneme uvést základní informace (datum, místo, …).
-   Pokud zadání obsahuje například formuli „článek do časopisu Květy“, můžeme začít formulací „Milé čtenářky časopisu Květy“. Dáme tím najevo, že jsme pochopili zadání (komunikační situaci).
-   Článek se často vyskytuje v zadání typu: „článek s prvky reportáže/fejetonu/…“ V takovém případě náš text musí obsahovat i prvky zmíněného útvaru.
-   Na rozdíl od zprávy **nemusíme být neustále objektivní**. Obzvláště u článku s prvky reportáže se po nás vyžaduje i subjektivní názor.
-   Často se vyskytující druhy článku:
    -   Článek s prvky reportáže (vyžaduje se náš vlastní názor na proběhlou akci/navštívené místo/…, píšeme jej v 1. osobě)
    -   Článek s prvky fejetonu (fejeton vtipně zpracovává aktuální a obvykle nevýznamné téma, napsání fejetonu vyžaduje smysl pro humor a trochu zkušeností)
-   Žádoucí je nějaký závěr nebo zakončení.
-   Minimální délka je **250 slov** (lepší je napsat 270, některá slova tvoří celek, který se počítá jen jednou).


---------------------------


## Příklad

### Zachraňte si život za hodinu

Představte si, že vás masový vrah zavřel do malé sklepní místnosti a řekl vám, že si pro vás za hodinu přijde. Vaším jediným způsobem, jak přežít, je nalezení klíče od zámku, ke kterému vede cesta v podobě šifer a nejrůznějších indicií. Takhle nějak vypadají instrukce k jedné z mnoha únikových her, které se v naší republice v posledních letech objevily.

Únikové hry budí zájem mezi všemi věkovými kategoriemi, jelikož jde o zábavu, při které musíte zapojit zejména mozkové závity a chápat souvislosti. Většinou připomíná reálné, až extrémní, životní situace. Nemusíte oplývat kondicí zdatného sportovce, abyste to zvládli.

Osobně jsem měla možnost zažít únikovou hru na vlastní kůži. Děj hry se odehrával v roce 1980, kdy v České republice vládli komunisté. Náš tým byl složen ze čtyř hráčů. Zavřeli nás do věznice a my měli hledat cestu ven. Pokaždé, když se objevíte v nějaké místnosti, musíte začít hledat zámky, ke kterým poté začnete hledat klíče. Některé zámky jsou na kód, takže je potřeba splnit jisté úkoly, abyste si mohli zámek otevřít. Úkoly se ze začátku zdají těžké, ale jakmile pochopíte systém, tak se vaše myšlení urychlí a vše lze lehce odhalit. Před vstupem do místnosti dostanete vysílačku pro případ, že si dlouho s něčím nebudete vědět rady. Vedoucí hry vám může poradit, čemu začít věnovat pozornost.

My jednu šifru řešili pomocí výšek udavačů, jelikož měly přiřazená písmenka. Pak stačilo z písmenek sestavit slovo a nápověda pro další klíč byla na světě. Hlavním cílem hry se nabízí možnost uniknout do limitu jedné hodiny. Osobně si ale myslím, že cílem je se zejména dobře pobavit a vyzkoušet si něco jedinečného.

Únikové hry se vytváří na různá témata. Jedním z nejzajímavějších je hra v  Plzni, odehrávající se v místnostech bývalého Černobylu. Vedle nás se zároveň odehrávala hra s tématem hororu. Tým uvěznili ve staré stodole a masový vrah si pro ně měl za 60 minut přijít. Bohužel, tým stihl uniknout až za 73 minut. S takovým časem by ve skutečném životě neuspěli.

Hry bych hodnotila jako skvělou zábavu, pokud chcete vyzkoušet svoji logiku v praxi a pobavit se s přáteli. Věřím, že jakmile navštívíte jednu hru, budete chtít zažít i další.



https://www.soom.cz/clanky/222--Clanek-o-tom-jak-psat-clanky