# Reportáž

[Reportáž - pravopisne.cz](https://www.pravopisne.cz/2019/01/slohovy-utvar-publicisticke-texty-zprava-reportaz-fejeton/#reportaz)

- styl **odborný** i **umělecký**
- úkolem je informovat zajímavě
- nabídnout čtenáři **co nejpřesnější představu** toho, co se odehrálo
- **autorovo očité svědectví**
- reportér **"hovoří z místa činu"**
- **titulek** (stejně jako zpráva)
	- zajímavý, poutavý, chytlavý (první věc co lidi vidí)
- tělo reportáže by mělo obsahovat stejné **informace** jako zpráva (_co, kdo, kdy, jak, proč, za kolik_)
- Text ovšem nebývá tak krátký jako zpráva, ale i zde je **závěr žádoucí**
- časté využívání **první osoby** *(Autor bývá účastníkem dění)*
- **rozhovory** účastníků
- **podrobný popis všímavého autora**
- Jazyk:
	- **jasné nejdůležitější informace**, ale může to být podáno jazykem lidštějším - **hovorovým (Ale stále spisovným!)**
	- v rozumné míře slova zabarvená
	- **hodnocení**
	- **přímá řeč**
- **popsat skutečnost přesně tak, jak je**


---------------------------


## Příklad

#### Přípravy

Kolem proudí davy lidí. Je pátek prvního června a za hodinu začíná celorepublikový závod trpaslíků. Bude to běh na 200 metrů. Jdu se podívat, jak se chystají. Většina se ještě převléká v šatně, ale někteří už čekají venku. „Jak dlouho jste se na závod připravoval?“ ptám se jednoho z nich. „Trénoval jsem náročně přes dva roky. Myslím, že mám velkou šanci vyhrát. Uvidíme, jestli to vyjde.“

#### Start

Trpaslíci jsou již připraveni na startovních pozicích a za pár okamžiků vše začne. Výstřel z pistole. Závodníci vyrážejí. Hned v prvních metrech se rozdělují na rychlejší a pomalejší, ale to ještě nic neznamená. Na výsledku závodu nerozhodne jen rychlost, ale i vytrvalost. Půlka závodu je za námi, zbývá 100 metrů.

#### Nehoda

  Ale ne! Jeden z trpaslíků zakopl a neudržel rovnováhu. Spadl do cesty jinému běžci. Oba leží na zemi. Na místo hned vyráží záchranáři. Jeden má zlomené předloktí, druhý vyvrknutý kotník. V závodě pokračovat nebudou.

#### Cíl

  Je to hodně vyrovnaný závod, ale vítězem se může stát jen jeden. Běžci se blíží k cíli. Vedení drží trpaslík č. 10, ale v patách má další dva. Bude to velmi těsné. Už zbývá jenom cílová rovinka. Všichni jsou téměř vyčerpáni. Komu vydrží síly déle? Je to velmi napínavé. Běžec s číslem 7 se dostal do vedení! Posledních pár metrů! Trpaslík č. 7 vítězí! Hned za ním do cíle dorazil č. 10! Jen o pár sekund pomalejší závodník s číslem 2 se umístil na třetí pozici.

#### Cena pro vítěze – Předávání cen

Nyní si všichni mohou dopřát zasloužený odpočinek. Následovat bude slavnostní předávání cen. Tři nejlepší čekají na stupních vítězů. Předává se bronzová, stříbrná, a nakonec i zlatá medaile pro mistra republiky. Ještě foto a závodníci můžou spokojeně odejít domů.
