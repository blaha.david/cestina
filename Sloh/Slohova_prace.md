# Slohová práce

[https://www.pravopisne.cz/category/slohove-prace/](https://www.pravopisne.cz/category/slohove-prace/)

[CJL_jaro_2019_PP.pdf](https://maturita.cermat.cz/files/files/testy-zadani-klice/CJL_jaro_2019_PP.pdf)
[CJL_podzim_2019_PP.pdf](https://maturita.cermat.cz/files/files/CJL/intaktni-zaci/MZ2019P/CJL_podzim_2019_PP.pdf)

## [Vypravování](/Sloh/Vypravovani.md)

## [Článek](/Sloh/Clanek.md)

## [Líčení](/Sloh/Liceni.md)

## [Dopis](/Sloh/Dopis.md)

## [Úvaha](/Sloh/Uvaha.md)

## [Zpráva](/Sloh/Zprava.md)

## [Reportáž](/Sloh/Reportaz.md)

## [Charakteristika](/Sloh/Charakteristika.md)











----------------------------------



## Kontrola slohových prací


