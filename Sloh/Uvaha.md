# Úvaha




---------------------------


## Příklad

### Řeka je časem

 Čas je v mnohém podobný řece. Plyne pořád jen dopředu stejně jako proud řeky a nikdy se nevrací zpět. Kajakáři se po řekách také plaví po proudu a ne proti.

 Řeka může v některých místech mít různou šířku a na ní závisí rychlost proudu. Jak je to časem? Ten přece nemůže být širší nebo užší. Ale když se dobře bavíme tak taky plyne rychleji a když se nudíme tak se zdá že se dopředu pohybuje krokem.

 Takže je opravdu řeka časem? Určitě ne. S motorovým člunem jde jet i proti proudu a nic podobného jsme pro čas ještě nevymysleli. Ale jsou si v určitých oblastech hodně podobné.