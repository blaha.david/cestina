# Zpráva
[Zpráva - prevopisne.cz)](https://www.pravopisne.cz/2019/01/slohovy-utvar-publicisticke-texty-zprava-reportaz-fejeton/#zprava)
[Zpráva - rozbor-dila.cz](https://rozbor-dila.cz/slohove-prace-slohovy-utvar-zprava/)

- setkáváme se s ním nejčastěji - v novinách, televizi, rádiu
- **informuje o něčem co se stalo**
- --> **minulý čas** *(budoucí čas --> oznámení, ne zpráva)*
- **objektivní** = informace, fakta *(ne osobní pocity)*
- **nejdůležitější informace patří na začátek**
	- _sestupná perspektiva_
- **titulek** - zajímavý, aby čtenáře oslovil *(donutit ho, aby si ho přečetl)*
- v textu **stručně** všechny důležité informace -  ***co, kdy, kde, jak,s kým***
- **neutrální jazyk bez citového zabarvení**

