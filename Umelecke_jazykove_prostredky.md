
[Umělecké jazykové prostředky - Wiki](https://wiki.rvp.cz/Kabinet/Ucebni_texty/ZZNetridene/Um%C4%9Bleck%C3%A9_jazykov%C3%A9_prost%C5%99edky)

[Básnické prostředky: 9 nejčastějších figur k maturitě](https://www.studentmag.cz/basnicke-prostredky-zopakujte-si-pred-maturitou-9-nejcastejsich-figur/)

**Sonet = 4433**

## Druhy lyriky

- přírodní lyrika
- milostná, intimní lyrika
- společenská lyrika
- náboženská lyrika
- meditativní lyrika
- reflexivní lyrika
- popisná lyrika
- apelativní lyrika
- kreativní lyrika
- vlastenecká lyrika

## Žánry

- elegie
- epigram
- epitaf
- pásmo
- kaligraf
- píseň
- blues
- óda
- hymnus

## Lyricko-epické žánry

_žánr = typ básně_

- poema
- balada
- romance

# Obrazná pojmenování

jedná se o _nepřímá pojmenování_, jež se používají hlavně v uměleckém slohu.

## Metafora

- v jediném pojmenování spojuje 2 významy na základě vnější podobnosti (_zub pily_, _kapka štěstí_, _hlava rodiny_, _perly rosy_)

## Metonymie

- spojení 2 významů na základě věcně souvislosti (_město čeká_ ⇒ všichni obyvatelé města)

## Personifikace

- zosobnění – neživým věcem jsou přisuzovány lidské vlastnosti a schopnosti (_moře mě objalo rukama_, _slunce se usmívá_)

## Synekdocha

- druh metonymie
- pojmenovává celek jednou z jeho částí (_žili pod jednou střechou_ ⇒ dům; _v létě tyčí se tu kukuřičná zrna_ ⇒ klasy)

## Perifráze

- opis, jímž se vystihuje určitý jev nebo děj pomocí typických znaků
- nepojmenovává věc přímo, ale prostřednictvím pojmenování hlavních vlastností či vzhledu (_přístroj Celsiův_ ⇒ teploměr)
- př. _„Nenadála jsem se, že se tak brzy u vás budou péci svatební koláče.&quot;_ ⇒ že budete tak brzy strojit svatbu

## Eufemismus

- zjemnění nepříjemné věci formou opisu (_až odejdu_ nebo _až bude růst nade mnou tráva_ ⇒ až umřu)

## Oxymoron

- nelogické spojení dvou protikladných slov (jejich významy se vylučují)
- má zdůraznit vlastnost (_živá mrtvola_, _umřelé hvězdy svit_)

## Ironie, sarkasmus

- vytýkání nedostatků pomocí slov opačného významu

...

## Apostrofa

- Básnické oslovení

## Anafora

- opakování stejného slova na začátku veršů

## Epifora

- opakování stejného slova na konci veršů

## Slovní inverze

- přeházený slovosled

---------------------------------------

**Prostěsdělovací styl** – styl běžné každodenní komunikace — hovorový (např. v rodině, mezi přáteli, ve škole, v zaměstnání apod.).

**Odborný styl** – styl odborných publikací, odborných časopisů a učebnic. Jeho cílem je podat přesné, jasné a relativně úplné informace z různých oborů lidské činnosti a poučit tím adresáta. Často se také používají odborné pojmy.

**Publicistický styl** – styl médií. Vyznačuje se spisovným jazykem, je proměnlivý a dynamický. Podává nové aktuální informace.

**Umělecký styl** – styl spisovatelů, zaměřuje se na funkci poznávací, estetickou a osobitost vyjádření.

**Administrativní styl** – styl informativní a věcné komunikace. Někdy se též označuje jako jednací či úřední. Je zaměřen především na fakta a na jejich sdělení.

**Alegorie = jinotaj** - vypovídá o něčem jiném než se zdá

Popis osoby - Charakteristika

Kontrast = Oxymoron - dvě opačné věci

**Lyrický subjekt** = autor vystupující v básni

**Lyrický objekt** = ten, ke komu se lyrický subjekt obrací

Básnický přívlastek = **epiteton** (*živý plamen*)

sloka básně = **strofa**

**apoziopeze** - nedokončená výpověď (pomlčky nebo tři tečky)

-----------------------------------------------

Seznam pojmů na ČJL

################

Legenda:

• když zmiňuji pojmy v textu vysvětlené jsou podtrhnuty

• # text # značí poznámku

• - vysvětlení: \_\_\_\_ značí krátké vysvětlení proč se jedná právě o daný jev

################

Literární formy:

1. Próza – běžný text – druhy: epika nebo drama

2. Poezie – báseň – druhy: lyrika, epika, lyricko-epická díla

Druhy literatury:

1. Lyrika – Důraz na city (autorovy city) (př. Písně z Lesbu)

2. Epika – Důraz na příběh díla (př. Beowulf)

3. Lyricko-epická díla – Dílo obsahuje příběh doplněn o city autora (př. Máj)

#Lyricko-epická díla nepatří do literárních druhů, ale patří k lyrice a epice tak to zmíním#

4. Drama – Dílo určené pro hru jakožto představení (př. Romeo a Julie)

Žánry lyriky:

1. Píseň – kratší báseň, pravidelný rytmus, výrazný rým – časté v lidové slovesnosti (doplněna o hudbu/hru na hudební nástroj)

(př. libovolná píseň, Anakreón: Písně)

2. Óda (hymnus – poddruh oslavuje boha/vlast/národ) – oslavná báseň/píseň

(př. Anakreón: Písně)

#Žánrem Písně Anakreóna padají spíše do písně, ale opěvují radosti života OSLAVUJÍ radosti života tedy by se daly zařadit i sem#

3. Elegie (= žalozpěv) – vyjadřuje smutek autora (často nad ztrátou něčeho blízkého)

(př. Ovidius: Žalozpěvy)

4. Žalm – náboženská díla – židi a křesťané si ji oblíbily

(př. Žalmy krále Davida ze Starého zákona)

5. Epigram – kritická, vtipná satirická báseň – ostrá kritika vůči tématu např. církev, společnost…

(př. Karel H. Borovský: Epigramy)

Básnické formy:

1. Sonet (= znělka)

2. Francouzská balada/villonská balada

3. Italská balada (= balatta)

Žánry lyricko-epické:

1. Balada

2. Romance

3. Poema

Žánry dramat:

1. Tragédie

2. Komedie (= veselohra)

3. Činohra

4. Fraška

5. Opera – hudba + zpěv

6. Opereta – předchůdce dnešních muzikálů – hudba + zpěv + dialogy

Kompozice:

1. Chronologická kompozice – Čas plyne v časové posloupnosti (běžně plyne) –

je přítomnost a během četby plyne čas – často vyprávění v přítomnosti

(př. „Je ráno. Vstávám. Jdu se najíst. Je noc. Usínám.&quot;)

2. Retrospektivní kompozice – Děj se odehrává v minulosti (částečně) – časté v detektivkách – vyšetřuje se v přítomnosti během kterého se autor zmiňuje o ději v minulosti

(př. „Potkal jsem kamaráda a vyprávěl jsem mu o včerejším testu&quot;)

3. Paralelní kompozice – Více dějů se děje naráz – často se proplétají

(př. Kapitola 1. je z pozice Michala a Kapitola 2. je z pozice Karla)

4. Zrcadlová kompozice – obsahuje velké množství kontrastů

(př. dílo Evžen Oněgin) #Jinde se téměř nepoužívá#

NEBO způsob řazení povídek (taktéž zrcadlová kompozice)

(př. dílo Kytice)

5. Rámcová kompozice – U povídkových knih – více povídek zařazuje do většího příběhu

(př. Dekameron)

#Můžete se setkat s gradací/retardací, ale někdy je řazena jakožto kompozice a někdy jakožto figura#

6. Gradace (opak retardace) – děj se s postupem času stává pro čtenáře zajímavější/napínavějším

(př. běžný příběh – úvod nezajímavý, konflikt zachytí čtenářovu pozornost (gradace, která je až do vrcholu), vrchol je nejnapínavější část příběhu pak nastává retardace a probíhá rozřešení příběhu a nakonec závěr)

Prostředky:

1. Tropy = obrazná pojmenování

Hlavní druhy:

1.1. Metafora – přenesení významu na základě vnější podobnosti

(př. „Hlava rodiny&quot; -vysvětlení: hlava vede v přeneseném významu ten, kdo vede je hlavou)

1.2. Metonymie – přenesení významu na základě vnitřní podobnosti

(př. Máj: „Hrdliččin zval ku lásce hlas&quot; - vysvětlení: hrdlička má hlas a hlas může někoho zvát)

Další dělení

1.1. Synestézie – prohození vjemu smyslového vnímání

(př. „ticho černé jako tvé vlasy&quot; – vysvětlení: ticho (= vjem sluchu) je využit k popisu barvy)

1.2. Přirovnání – porovnání, pozná se pomocí slova jako

(často jediný rozdíl mezi metaforou a přirovnáním v přirovnáním je jako)

(př. „Černé jako noc&quot;)

1.3. Personifikace – zvířeti či neživému předmětu jsou přiřazeny lidské (v případě předmětů i zvířecí) vlastnosti

(př. postava kocour Mikeš, žánr bajky)

1.4. Oxymóron – slovní spojení, které nedává ve výsledku smysl

(př. Máj: „zbortěné harfy tón&quot; - vysvětlení: zbortěná harfa již nemůže vydávat tón)

1.5. Synekdocha – celek je nahrazen jednotlivcem či naopak jednotlivec je nahrazen celkem

(př. „nepřišla ani noha&quot; – vysvětlení: noha nahrazuje celé osoby)

1.6. Hyperbola (= nadsázka) – zveličení skutečnosti

(př. „Neviděl jsem tě tisíce let!&quot;)

1.7. Eufemismus (= zjemnění) – oslabení skutečnosti

(př. „opustil nás&quot; -\&gt; umřel)

1.8. Ironie – vyjádření, kde smysl je přesně opačný k dané skutečnosti

(př. Za deště říkat: „pěkné počasí, co?&quot; #pokud nemáte rádi déšť#)

1.9. Symbol – znak, který zastupuje obecný pojem

(př. srdce -\&gt; symbol lásky)

1.10. Epiteton (někdy Epiteta) (= básnický přívlastek) – dva druhy (viz pozn.) – od obyčejného přívlastku má navíc subjektivní, citové hodnocení

(př. „vzteklý oheň&quot; – vysvětlení: vztek je emoce a vyvolává v tomto spojení ve čtenáři emoce, které do tohoto výrazu vložil autor)

#Druhy nejsou potřeba zatím, ale do budoucna je dávám – zatím k nim nedávám příklady, protože je po nás zatím nechtěla rozlišovat#

1.10.1. Epiteton Ornans (= zdobný) – působí ozdobně a zároveň dodává podstatnému jménu novou vlastnost

1.10.2. Epiteton Constans (= ustálený) – nějaký přívlastek vycházející z něčeho známého např. z historie, báje

2. Figury (básnické) #Setkáváte se s většinou pouze v básni tučně budou figury i do běžného textu#

2.1. Aliterace – opakování hlásek na počátku po sobě následujících slov

(př. „plyne peníz po penízku&quot;)

2.2. Onomatopoie (= zvukomalba) – seskupení hlásek tak, aby to čtenáři připadalo jako zvuk, který to popisuje

(př. Máj: „Nocí řinčí řetězů hřmot&quot;)

2.3. Anafora – opakování slov (slovních spojení/sousloví) na počátku po sobě následujících nebo blízkých celků (začátek následujících veršů je nejčastější)

(př. „Smrt, nejkratší jméno podstatné!

Smrt, na tu si platíš předplatné!&quot;)

2.4. Epifora – opakování slov (slovních spojení/sousloví) na konci po sobě následujících nebo blízkých celků (konec následujících veršů je nejčastější)

(př. Kytice – Svatební košile: „Co to máš na té tkaničce?

Na krku na té tkaničce!&quot;)

2.5. Epanastrofa – opakování slov (slovních spojení/sousloví) po sobě následujících nebo blízkých celků tak, že první je na konci celku a druhé je na začátku celku následujícího (celek je nejčastěji verš, ale je i možno opakovat konec sloky na začátku následující sloky)

(př. „slova jsou jako kameny,

kameny! V kterých je člověk zazděný&quot;)

2.6. Epizeuxis – opakování dvou slov za sebou

(př. „Draci, draci! Jsou to holt divná stvoření&quot;)

2.7. Antiteze – stavění protikladných slov za sebou – příklady běžných antitezích: peklo-nebe, bílá-černá, noc-den… (tedy jestliže vidíte nějaký kontrast vedle sebe jedná se o antitezi)

(př. „Příliš černý pro nebesa, ale stále příliš bílý pro peklo&quot;)

2.8. Řečnické figury:

2.8.1. Apostrofa (= básnické/řečnické oslovení) – oslovení v básni = apostrofa

#učitelka nás učí, že oslovení v básni = apostrofa -\&gt; tak to komplikovat nebudeme, a tak to platí, i když to dle nějakých zdrojů tak není, ale komplikovat to nemá smysl#

(př. „hoj, ty Štědrý večere, ty tajemný svátku&quot;)

2.8.2. Řečnická otázka a odpověď – autor se ptá čtenáře bez toho, aby očekával od něj odpověď zpátky a někdy si odpoví řečnickou odpovědí na svoji řečnickou otázku

# Slohové útvary

[https://www.studuju.cz/latka-337](https://www.studuju.cz/latka-337)

– **automatizace** = využití předpokládaných ustálených výrazů; rychle informuje, umožňuje orientaci v textu (např. soud byl odročen, pachatel byl dopaden…); automatizace výrazů často vede k frázovitosti

– **aktualizace** = užití neobvyklého, nového výrazu k upoutání čtenáře a oživení textu

– způsoby aktualizace:

- ve spisovném textu se užívají výrazy z jiné stylové roviny (např. hovorové – tatrováci, lidové frazeologické obraty, výrazy knižní, zastaralé), dávají se do uvozovek
- užívají se slova v nezvyklých spojeních
- obměňují se frazeologická spojení
- kontrast
- slovní hříčky
- obrazné vyjadřování
- aktualizace ve větné stavbě – užití elipsy, subjektivního pořádku slov, jmenných a slovesně jmenných konstrukcí, nevlastních předložek

– opakovaná **aktualizace** se časem mění v **automatizaci**


**epizeuxis:** - opakování za sebou
jen ještě **lístečku**, **lístečku** maličký

 **elipsa** = **nedokončená výpověď** = přerušení řeči postavy
v textu označeno pomlčkou (někdy také tři tečky)