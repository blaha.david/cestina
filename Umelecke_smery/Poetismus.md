## Poetismus

Takže test je na poetismus, surrealismus a proletářskou poezii, což by mělo znamenat autory:
- Seifert + Nezval za poetismus
- Wolker + Biebl (kterého jsme měli nastudovat sami za DÚ) za proletářskou poezii
- A surrealismus je spíše zahraniční a tam byl hlavně Breton
- ale jinak se k surrealismu řadí i Nezval, ale on je prakticky poetismus (svou hravostí) dost podobný surrealismu
- Ještě by bylo fajn si pamatovat autora Apollinaire (1. pád), protože ten ten surrealismus pojmenoval (i když se sám do něj neřadí, jestli se nepletu)

## [Vítězslav Nezval](/Autori/Vitezslav_Nezval.md)

## [Jaroslav Seifert](/Autori/Jaroslav_Seifert.md)

