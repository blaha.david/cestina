# Ztracená generace
-   skupina **amerických** autorů narozených kolem roku 1900
-   20\. léta 20. století
-   zažili 1. světovou válku (zobrazují ji ve svých dílech)
-   popisují pocity vojáků po návratu z války (fyzicky i psychicky zmrzačeni)
-   ztratili mládí
-   ztratili kamarády
-   problémy s opětovným zařazením do společnosti (jsou ztraceni - nevědí, kde jsou)
-   tématika:
	-   zklamání a skepse
	-   rozklad lidských a společenských hodnot
	-   hledání východiska v útěku do přírody nebo kultury


## [Ernest Hemingway](/Autori/Ernest_Hemingway.md)
-   dobrovolník u červeného kříže za 1. sv. války
    

## Francis Scott Fitzegerald
-   1896 - 1940
-   Spisovatel
-   dobrovolník za 1. sv. války
-   *Velký Gatsby*
-   *Povídky jazzového věku*

## John Steinbeck


## [Erich Maria Remarque](/Autori/Erich_Maria_Remarque.md)
-   není američan - němec
-   nezařazuje se do skupiny ztracené generace